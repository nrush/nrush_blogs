```
├── 404.html
├── Gemfile
├── Gemfile.lock
├── _config.yml
├── _data
├── _drafts
    └── vfs
        └── 2023-3-26-Virtual-File-System.md
├── _includes
├── _layouts
├── _posts
├── _site
├── categories
├── css
├── fonts
├── index.html
├── js
├── readme.md
├── run_web.sh
├── scripts
├── search.json
└── sources
```