---
layout: default
title: softirq
categories: interrupt
author: Nrush
create_time: 2023-04-16 13:57:45
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

## 1 softirq机制简述

中断是系统中重要的组成部分，然而在中断中执行过多的代码不是一个好的选择，这会影响系统的响应速度和并发能力，另一方面，非嵌套状态下，处于硬中断上下文的处理器无法响应其他中断，这会导致丢中断。原则上硬中断上下文中的代码应当越好，但现实情况是我们的确需要在中断来临后做大量工作，这就产生中断上半部和下半部的概念，上半部表示在中断到来时必须处理的事务，下半部表示可以适当延后的工作。举个不是特别恰当的例子，工人使用机器生产工件，这时机器突然故障（中断），因为故障机器可能会导致人身事故，此时工人需要立刻关闭机器电源（上半部），停机后工人需要检修机器恢复生产（下半部）。关停机器必须立刻执行，那何时恢复生产呢？如果工期紧急，那可能需要越快恢复越好，如果工期宽松，那隔日修复也没问题。中断的上半部处理的是在中断的上下文中完成的，但下半部在何时处理就是一个值得讨论的时机。例如刚刚机器故障的例子，为了更灵活的处理中断的下半部，linux提供了softirq、tasklet、中断线程化和workqueue等方式，其响应及时程度依次递减。当然这三种途径不仅可以处理中断下半部，也可以作为异步通知手段。

## 2 softirq使用方法

softirq对外暴露的接口如下
```c
/* 注册一个软中断handler,当软中断被raise时，该handler会被执行 */
void open_softirq(int nr, void (*action)(struct softirq_action *));
/* 唤起软中断号为nr的softirq */
void raise_softirq(unsigned int nr);
/* 同上，但该api必须在关中断的情况下调用 */
void raise_softirq_irqoff(unsigned int nr);
```

NOTE：中断回调函数中不一定是关中断的，因为中断有可能被线程化（RT patch），在回调函数中raise softirq时需谨慎，不要默认回调函数的上下文是关中断的而直接调用`raise_softirq_irqoff()`。

## 3 softirq实现原理

softirq的实现源码位于kernel/softirq.c，首先看一下softirq的关键数据结构
```c
typedef struct {
	unsigned int __softirq_pending;
} ____cacheline_aligned irq_cpustat_t;
DEFINE_PER_CPU_ALIGNED(irq_cpustat_t, irq_stat);

struct softirq_action
{
	void	(*action)(struct softirq_action *);
};
static struct softirq_action softirq_vec[NR_SOFTIRQS] __cacheline_aligned_in_smp;
```
softirq相关的数据结构比较简单，所有软中断的handler都会由`open_softirq(hanler, nr)`注册到对应的数组位置，因此这个`softirq_vec[]`可以理解为中断向量表。
```c
void open_softirq(int nr, void (*action)(struct softirq_action *))
{
	softirq_vec[nr].action = action;
}

/* 目前内核中使用的softirq */
enum
{
	HI_SOFTIRQ=0,
	TIMER_SOFTIRQ,
	NET_TX_SOFTIRQ,
	NET_RX_SOFTIRQ,
	BLOCK_SOFTIRQ,
	IRQ_POLL_SOFTIRQ,
	TASKLET_SOFTIRQ,
	SCHED_SOFTIRQ,
	HRTIMER_SOFTIRQ,
	RCU_SOFTIRQ,    /* Preferable RCU should always be the last softirq */
	NR_SOFTIRQS
};
```
per_cpu变量`irq_stat`被用来表示本cpu的软中断状态，变量`__softirq_pending`的每一位bit都代表对应软中断状态，软中断发生时会调用`raise_softirq()`，置位对应的bit，软中断处理完后对应bit会被清除，这个`irq_stat`相当于per_cpu的中断寄存器。
```c
inline void raise_softirq_irqoff(unsigned int nr)
{
	__raise_softirq_irqoff(nr);

	/*
	 * If we're in an interrupt or softirq, we're done
	 * (this also catches softirq-disabled code). We will
	 * actually run the softirq once we return from
	 * the irq or softirq.
	 *
	 * Otherwise we wake up ksoftirqd to make sure we
	 * schedule the softirq soon.
	 */
	if (!in_interrupt() && should_wake_ksoftirqd())
		wakeup_softirqd();
}

void raise_softirq(unsigned int nr)
{
	unsigned long flags;

	local_irq_save(flags);
	raise_softirq_irqoff(nr);
	local_irq_restore(flags);
}

void __raise_softirq_irqoff(unsigned int nr)
{
	lockdep_assert_irqs_disabled();
	trace_softirq_raise(nr);
	or_softirq_pending(1UL << nr);
}
```
从上面源码看，raise一个softirq必须是关硬中断，原因是什么？这需要分析softirq的处理流程。处理softirq的时机有多处，per_cpu的softirqd线程上下文、硬中断执行`irq_exit()`时、`local_bh_enable()`使能底半部时都会触发softirq处理。先来分析硬中断softirq执行流程：
```c
中断-->irq_exit()-->__irq_exit_rcu()-->invoke_softirq()
```
硬中断结束后，会判断是否有本地的软中断被置位，如置位则开始调用`invoke_softirq()`进行软中断处理流程
```c
if (!in_interrupt() && local_softirq_pending())
	invoke_softirq();
```
对于开启抢占的内核(`CONFIG_PREEMPT_RT`)，处理流程比较简单，直接唤醒本地的`softirqd`处理
```c
static inline void invoke_softirq(void)
{
	if (should_wake_ksoftirqd())
		wakeup_softirqd();
}
```
何时唤醒`softirqd`？
```c
static inline bool should_wake_ksoftirqd(void)
{
	return !this_cpu_read(softirq_ctrl.cnt);
}
```
这里的`cnt`是用来记录，当前是否处于关软中断的状态（非0关闭，0未关闭），若处于关软中断状态，则此处应该避免无意义的唤醒动作。
```
QUESTION：RT 为什么在这里修改了处理流程？
```
对于未开启抢占的内核，此处处理逻辑会复杂些
```c
static inline void invoke_softirq(void)
{
	if (ksoftirqd_running(local_softirq_pending()))
		return;

	if (!force_irqthreads() || !__this_cpu_read(ksoftirqd)) {
		__do_softirq();
	} else {
		wakeup_softirqd();
	}
}
```
- line 3 判断softirqd是否在运行，运行则让softirqd处理软中断，跳过该处理
- line 6 如果没有开启中断线程化或ksoftirqd还未初始化成功，则直接在硬中断处理流程中处理软中断，否则唤醒softirqd处理。

中断线程化一般在高实时性要求场合下会开，可见内核开发者并不希望高实时性要求要求场合下在中断退出流程中处理软中断。

继续分析软中断的具体处理流程

```c
void __do_softirq(void)
{
	unsigned long end = jiffies + MAX_SOFTIRQ_TIME;
	unsigned long old_flags = current->flags;
	int max_restart = MAX_SOFTIRQ_RESTART;
	struct softirq_action *h;
	bool in_hardirq;
	__u32 pending;
	int softirq_bit;

	/*
	 * Mask out PF_MEMALLOC as the current task context is borrowed for the
	 * softirq. A softirq handled, such as network RX, might set PF_MEMALLOC
	 * again if the socket is related to swapping.
	 */
	current->flags &= ~PF_MEMALLOC;

	pending = local_softirq_pending();

	softirq_handle_begin();
	in_hardirq = lockdep_softirq_start();
	account_softirq_enter(current);

restart:
	/* Reset the pending bitmask before enabling irqs */
	set_softirq_pending(0);

	local_irq_enable();

	h = softirq_vec;

	while ((softirq_bit = ffs(pending))) {
		unsigned int vec_nr;
		int prev_count;

		h += softirq_bit - 1;

		vec_nr = h - softirq_vec;
		prev_count = preempt_count();

		kstat_incr_softirqs_this_cpu(vec_nr);

		trace_softirq_entry(vec_nr);
		h->action(h);
		trace_softirq_exit(vec_nr);
		if (unlikely(prev_count != preempt_count())) {
			pr_err("huh, entered softirq %u %s %p with preempt_count %08x, exited with %08x?\n",
			       vec_nr, softirq_to_name[vec_nr], h->action,
			       prev_count, preempt_count());
			preempt_count_set(prev_count);
		}
		h++;
		pending >>= softirq_bit;
	}

	if (!IS_ENABLED(CONFIG_PREEMPT_RT) &&
	    __this_cpu_read(ksoftirqd) == current)
		rcu_softirq_qs();

	local_irq_disable();

	pending = local_softirq_pending();
	if (pending) {
		if (time_before(jiffies, end) && !need_resched() &&
		    --max_restart)
			goto restart;

		wakeup_softirqd();
	}

	account_softirq_exit(current);
	lockdep_softirq_end(in_hardirq);
	softirq_handle_end();
	current_restore_flags(old_flags, PF_MEMALLOC);
}
```


- line 18 读取当前softirq的软中断pending状态，本次将会依次处理完。
- line 20 标记调用`	__local_bh_disable_ip()`软中断处理开始，主要关闭上下文处理的下半部处理流程进入软中断临界区。
- line 22 标记当前task进入软中断，用于统计软中断耗时
- line 26-28 获取当前cpu软中断的pending位，并开启中断。可见软中断处理过程中是允许硬中断产生的，硬中断handler中有可能会raise软中断，如果在开启硬中断前未清空pending位，就有可能漏掉软中断。注意本次要处理的pending位已在line 18处缓存。
- line 30-54 遍历pending位依次处理软中断回调函数。
- line 60-69 关中断，重新读取pending位，如果软中断处理流程中有软中断发生，且处理流程没有超过MAX_SOFTIRQ_TIME(2ms)、没有调度请求、没有超过最大处理次数(10)，则返回restart处再次重复处理软中断流程。否则唤醒softirqd，交由softirqd处理。
- line 71-75 退出软中断处理流程。

softfirqd以及local_bh_enable()中的软中断处理流程中主体部分都调用的是`__do_softirq()`函数，这里不在详细分析。

## 4. 要点总结

- 当前内核中softirq数目是有限的，已使用的有10个，网络收发、timer回调、hrtimer回调、tasklet回调、sched负载均衡回调都是在softirq中处理的。
- softirq可以在任意上下文被raise，但必须关中断。
- softirq可能会在中断退出时、退出下半部临界区以及softirqd中被处理。
- softirq的处理是percpu的，在那个cpu上被唤醒讲由哪个cpu处理。
- softirq的handler的处理过程是开中断的，可以被中断打断，但不会被其他软中断中断。
- softirq的handler中也应当遵循短快、无休眠的原则。如果在softirq中休眠的话，其中一个影响就是可能阻塞softirqd线程，导致其他软中断无法被处理。
- softirqd可能被RT线程抢占，其有有限级为normal 120

{% highlight c linenos %}
    sss
{% endhighlight %}