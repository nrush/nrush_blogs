---
layout: default
title: kernel开发基本环境配置
categories: kernel-develop-env
author: Nrush
create_time: 2024-09-16 23:45:47
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

## FAQ:
### 如何反编译dtb文件？
```
./scripts/dtc/dtc -I dtb -O dts <input dtb> -o <output dts>
```

### QEMU如何启动QEMU mon调试？
```
参考：https://www.qemu.org/docs/master/system/introduction.html
在启动参数中添加：-serial mon:stdio \
使用：ctlrl+a c 进入QEMU monitor
```

## Reference
