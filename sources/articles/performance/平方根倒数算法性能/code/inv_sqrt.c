// #include <stdio.h>
// #include <math.h>

float inv_sqrt(float x) {
    float xhalf = 0.5f * x;
    int i = *(int*)&x; // get bits for floating VALUE
    i = 0x5f3759df - (i >> 1); // gives initial guess y0
    x = *(float*)&i; // convert bits BACK to float
    x = x * (1.5f - xhalf * x * x); // Newton step, repeating increases accuracy
    return x;
}

int main(int argc, char *argv[]) {
    float x = 4.0f;
    float result = inv_sqrt(x);
    // printf("1/sqrt(%f) = %f\n", x, result);
    return 0;
}