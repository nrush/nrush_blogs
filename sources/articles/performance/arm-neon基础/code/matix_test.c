#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/random.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#define COL_SIZE 4096
#define ROW_SIZE 2160
typedef uint16_t pixel_t;

void gen_random_pixel_data(char *path)
{
    size_t buffer_size_in_bytes = COL_SIZE * ROW_SIZE * sizeof(pixel_t);
    void *buffer = malloc(buffer_size_in_bytes);
    int fd;

    if (!buffer)
    {
        perror("failed to malloc buffer.\n");
        return;
    }

    ssize_t ret = 0;
    ret = getrandom(buffer, buffer_size_in_bytes, GRND_RANDOM);
    if (ret == -1)
    {
        perror("failed to get radom data.\n");
        goto error;
    }

    fd = open(path, O_CREAT | O_RDWR, S_IRWXU);
    if (fd == -1)
    {
        perror("failed to create file.\n");
        goto error;
    }

    ret = write(fd, buffer, buffer_size_in_bytes);
    if (ret == -1)
    {
        perror("failed to write file\n");
    }

error:
    free(buffer);
}

void *load_pixel_data(char *path)
{
    size_t buffer_size_in_bytes = COL_SIZE * ROW_SIZE * sizeof(pixel_t);
    int fd = 0;
    ssize_t ret;
    void *buffer = malloc(buffer_size_in_bytes);

    fd = open(path, O_RDWR);
    if (fd == -1)
    {
        perror("failed to create file.\n");
        return NULL;
    }

    ret = read(fd, buffer, buffer_size_in_bytes);
    if (ret != buffer_size_in_bytes)
    {
        perror("failed to read data.\n");
        free(buffer);
        return NULL;
    }

    return buffer;
}

size_t numberof_highlight_pixels(void *buffer, pixel_t threshold, size_t col_step, size_t row_step)
{
    size_t col = 0, row = 0, num = 0;
    pixel_t *image = (pixel_t *)buffer;

    for (row = 0; row < ROW_SIZE; row += row_step)
    {
        for (col = 0; col < COL_SIZE; col += col_step)
        {
            if (image[row * ROW_SIZE + col] > threshold)
            {
                num++;
            }
        }
    }

    return num;
}

size_t numberof_highlight_pixels_with_prefetch(void *buffer, pixel_t threshold, size_t col_step, size_t row_step)
{
    size_t col = 0, row = 0, num = 0;
    pixel_t *image = (pixel_t *)buffer;
    size_t index = 0;
    size_t row_base;

    for (row = 0; row < ROW_SIZE; row += row_step)
    {
        row_base = row * ROW_SIZE;
        // __builtin_prefetch(&image[row_base], 0, 1);
        for (col = 0; col < COL_SIZE; col += col_step)
        {
            // index = row*ROW_SIZE + col;
            if (image[row_base + col] > threshold)
            {
                num++;
            }
        }
    }

    return num;
}

int main(int argc, char **argv)
{
    size_t num = 0;
    void *buffer = NULL;
    size_t col_step, row_step;
    clock_t start, stop;

    // gen_random_pixel_data("./data");
    buffer = load_pixel_data("./data");

    col_step = 1;
    row_step = 1;
    start = clock();
    num = numberof_highlight_pixels(buffer, UINT16_MAX / 2, col_step, row_step);
    stop = clock();

    col_step = 1;
    row_step = 1;
    start = clock();
    num = numberof_highlight_pixels(buffer, UINT16_MAX / 2, col_step, row_step);
    stop = clock();

    printf("[+] The number of highlight pixels is %d [total %d pixels, col_step %d, row_step %d]\n", num, COL_SIZE * ROW_SIZE, col_step, row_step);
    printf("[+] The numberof_highlight_pixels takes %.2f us.\n", (double)(stop - start) / CLOCKS_PER_SEC * 1000000);

    col_step = 1;
    row_step = 1;
    start = clock();
    num = numberof_highlight_pixels(buffer, UINT16_MAX / 2, col_step, row_step);
    // num = numberof_highlight_pixels_with_prefetch(buffer, UINT16_MAX / 2, col_step, row_step);
    stop = clock();

    printf("[+] The number of highlight pixels is %d [total %d pixels, col_step %d, row_step %d]\n", num, COL_SIZE * ROW_SIZE, col_step, row_step);
    printf("[+] The numberof_highlight_pixels_with_prefetch takes %.2f us.\n", (double)(stop - start) / CLOCKS_PER_SEC * 1000000);

    return 0;
}