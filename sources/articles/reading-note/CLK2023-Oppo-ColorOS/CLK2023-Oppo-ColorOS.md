---
layout: default
title: CLK2023 Oppo ColorOS
categories: reading-note
author: Nrush
create_time: 2023-11-05 15:53:31
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

视频链接：https://live.csdn.net/room/csdnnews/roru5arM （34：00左右开始）
ppt: <a href="/sources/articles/reading-note/CLK2023-Oppo-ColorOS/refs/Keynote1-ColorOS下基于用户体验改善的Linux内核优化——Omega.pptx" download="test.jpg">点击下载</a>

目标：用户体验为中心，流畅、安全、智能高效。
问题：
1. cpu资源调度
   1. CFS实时性问题：ui/render nice值提升，重载仍然有实时性问题。
   2. 负载追追踪不及时：
   3. 实时调度的负载追踪：显示的rt线程重载场景下
   4. 功耗和响应的问题
2. 内存问题
   1. 内存泄漏
   2. 内存碎片化
   3. 内存分配的实时性
   4. 内存回收负载高
3. 锁竞争


## Reference
