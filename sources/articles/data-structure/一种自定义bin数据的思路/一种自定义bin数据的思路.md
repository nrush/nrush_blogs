---
layout: default
title: 一种自定义bin数据的思路
categories: data-structure
author: Nrush
create_time: 2023-04-05 16:31:16
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

&emsp;&emsp;在工作过程中，需要将一部份文本数据转化为bin数据便于存储和c程序去处理。当然C程序直接处理文本文件也是没问题的，但从以下两个角度考虑，还是转化为bin格式去处理更高效一些。

- C语言处理文本并不方便。
- 这些数据需要多次被使用，每次都处理一边效率低。
- 这些数据需要在服务器上进行存储，bin格式体积会更小一些。

存储的bin格式需要满足以下需求：
- c程序高效写入导出

linux下和文件交互的最简单高效的方式是mmap，从这点出发，将要存储的数据按内存布局直接dump到bin文件中，再由mmap将文件加载到内存中无疑是最方便的。**<u>核心点：把文件当成内存来看，并以此组织数据</u>**。具体实现如下：

&emsp;&emsp;考虑一组可以用如下c程序表达的数据:
```c
/*****************************
 * bit width: 32bit
 *  struct item {
 *      int y;
 *      char *s;
 *  };
 *  
 *  struct data {
 *      int x;
 *      int number_of_item;
 *      struct item *item;
 *      char *s;
 *  };
 *****************************/

struct item it = {
    .y = 1,
    .s = "haa"
};

struct data dat = {
    .x = 2,
    .number_of_item = 1
    .item = &it,
    .s = 'waa'
};
```
那么可以构造如下的bin数据存储结构进行存储
<center><img src="/sources/articles/data-structure/一种自定义bin数据的思路/img/bin_format.svg" alt="bin_format" width=650 onerror="this.src='/sources/articles/data-structure/一种自定义bin数据的思路/img/bin_format.jpg'"/></center>

其中c结构中各个指针值都被表示成了对应数据在bin文件中的偏移。加载时只需要mmap文件，拷贝到分配好的内存中，然后根据bin数据的提供的offset值来计算各个指针在内存中的真实地址，然后修改对应指针的值为该真实地址即可，下面是一段处理的伪代码
```c
struct data_header {
    int version;
    int number_of_data;
    struct data *data;
    uint32_t item_offset;
    uint32_t string_offset;
};

dat = mmap('./dat');
buf = malloc(size_of_data);
memcpy(dat, buf, size_of_data);
struct data_header *header = (struct data_header *)dat;

for (i = 0; i < header->number_of_data; i++) {
    struct data *dat_ptr;
    dat_ptr->item = (uint32_t)dat_ptr->item + (uint32_t)header;
    dat_ptr->s = (uint32_t)dat_ptr->s + (uint32_t)header;
    for (j = 0; j < dat_ptr->number_of_item; j++) {
        struct item *it_ptr;
        it_ptr->s = (uint32_t)it_ptr->s + (uin32_t)header;
    }
}
```
