---
layout: default
title: Linux内存基�?�
categories: memory
author: Nrush
create_time: 2024-01-14 19:28:16
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

本文所有内容基于ARM64架构，6.2版本分析

## 1 内存初始化

### 1.1 进入内核前的内存

参考：Documentation/translations/zh_CN/arm64/booting.txt
在内核启动之前，bootloader至少要完成如下工作：

1、设置和初始化 RAM【必须】
2、设置设备树数据【必须】
3、解压内核映像【可选】
4、调用内核映像【必须】

具体每步的要求可以参考上述文档，这里直接贴下原文：
```
基本上，引导装载程序（至少）应实现以下操作：

1、设置和初始化 RAM
2、设置设备树数据
3、解压内核映像
4、调用内核映像


1、设置和初始化 RAM
-----------------

必要性: 强制

引导装载程序应该找到并初始化系统中所有内核用于保持系统变量数据的 RAM。
这个操作的执行方式因设备而异。（它可能使用内部算法来自动定位和计算所有
RAM，或可能使用对这个设备已知的 RAM 信息，还可能是引导装载程序设计者
想到的任何合适的方法。）


2、设置设备树数据
---------------

必要性: 强制

设备树数据块（dtb）必须 8 字节对齐，且大小不能超过 2MB。由于设备树
数据块将在使能缓存的情况下以 2MB 粒度被映射，故其不能被置于必须以特定
属性映射的2M区域内。

注： v4.2 之前的版本同时要求设备树数据块被置于从内核映像以下
text_offset 字节处算起第一个 512MB 内。

3、解压内核映像
-------------

必要性: 可选

AArch64 内核当前没有提供自解压代码，因此如果使用了压缩内核映像文件
（比如 Image.gz），则需要通过引导装载程序（使用 gzip 等）来进行解压。
若引导装载程序没有实现这个功能，就要使用非压缩内核映像文件。


4、调用内核映像
-------------

必要性: 强制

已解压的内核映像包含一个 64 字节的头，内容如下：

  u32 code0;			/* 可执行代码 */
  u32 code1;			/* 可执行代码 */
  u64 text_offset;		/* 映像装载偏移，小端模式 */
  u64 image_size;		/* 映像实际大小, 小端模式 */
  u64 flags;			/* 内核旗标, 小端模式 *
  u64 res2	= 0;		/* 保留 */
  u64 res3	= 0;		/* 保留 */
  u64 res4	= 0;		/* 保留 */
  u32 magic	= 0x644d5241;	/* 魔数, 小端, "ARM\x64" */
  u32 res5;			/* 保留 （用于 PE COFF 偏移） */


映像头注释：

- 自 v3.17 起，除非另有说明，所有域都是小端模式。

- code0/code1 负责跳转到 stext.

- 当通过 EFI 启动时， 最初 code0/code1 被跳过。
  res5 是到 PE 文件头的偏移，而 PE 文件头含有 EFI 的启动入口点
  （efi_stub_entry）。当 stub 代码完成了它的使命，它会跳转到 code0
  继续正常的启动流程。

- v3.17 之前，未明确指定 text_offset 的字节序。此时，image_size 为零，
  且 text_offset 依照内核字节序为 0x80000。
  当 image_size 非零，text_offset 为小端模式且是有效值，应被引导加载
  程序使用。当 image_size 为零，text_offset 可假定为 0x80000。

- flags 域 (v3.17 引入) 为 64 位小端模式，其编码如下：
  位 0: 	内核字节序。 1 表示大端模式，0 表示小端模式。
  位 1-2:	内核页大小。
			0 - 未指定。
			1 - 4K
			2 - 16K
			3 - 64K
  位 3:		内核物理位置
			0 - 2MB 对齐基址应尽量靠近内存起始处，因为
			    其基址以下的内存无法通过线性映射访问
			1 - 2MB 对齐基址可以在物理内存的任意位置
  位 4-63:	保留。

- 当 image_size 为零时，引导装载程序应试图在内核映像末尾之后尽可能
  多地保留空闲内存供内核直接使用。对内存空间的需求量因所选定的内核
  特性而异, 并无实际限制。

内核映像必须被放置在任意一个可用系统内存 2MB 对齐基址的 text_offset
字节处，并从该处被调用。2MB 对齐基址和内核映像起始地址之间的区域对于
内核来说没有特殊意义，且可能被用于其他目的。
从映像起始地址算起，最少必须准备 image_size 字节的空闲内存供内核使用。
注： v4.6 之前的版本无法使用内核映像物理偏移以下的内存，所以当时建议
将映像尽量放置在靠近系统内存起始的地方。

任何提供给内核的内存（甚至在映像起始地址之前），若未从内核中标记为保留
(如在设备树（dtb）的 memreserve 区域），都将被认为对内核是可用。

在跳转入内核前，必须符合以下状态：

- 停止所有 DMA 设备，这样内存数据就不会因为虚假网络包或磁盘数据而
  被破坏。这可能可以节省你许多的调试时间。

- 主 CPU 通用寄存器设置
  x0 = 系统 RAM 中设备树数据块（dtb）的物理地址。
  x1 = 0 (保留，将来可能使用)
  x2 = 0 (保留，将来可能使用)
  x3 = 0 (保留，将来可能使用)

- CPU 模式
  所有形式的中断必须在 PSTATE.DAIF 中被屏蔽（Debug、SError、IRQ
  和 FIQ）。
  CPU 必须处于 EL2（推荐，可访问虚拟化扩展）或非安全 EL1 模式下。

- 高速缓存、MMU
  MMU 必须关闭。
  指令缓存开启或关闭皆可。
  已载入的内核映像的相应内存区必须被清理，以达到缓存一致性点（PoC）。
  当存在系统缓存或其他使能缓存的一致性主控器时，通常需使用虚拟地址
  维护其缓存，而非 set/way 操作。
  遵从通过虚拟地址操作维护构架缓存的系统缓存必须被配置，并可以被使能。
  而不通过虚拟地址操作维护构架缓存的系统缓存（不推荐），必须被配置且
  禁用。

  *译者注：对于 PoC 以及缓存相关内容，请参考 ARMv8 构架参考手册
   ARM DDI 0487A

- 架构计时器
  CNTFRQ 必须设定为计时器的频率，且 CNTVOFF 必须设定为对所有 CPU
  都一致的值。如果在 EL1 模式下进入内核，则 CNTHCTL_EL2 中的
  EL1PCTEN (bit 0) 必须置位。

- 一致性
  通过内核启动的所有 CPU 在内核入口地址上必须处于相同的一致性域中。
  这可能要根据具体实现来定义初始化过程，以使能每个CPU上对维护操作的
  接收。

- 系统寄存器
  在进入内核映像的异常级中，所有构架中可写的系统寄存器必须通过软件
  在一个更高的异常级别下初始化，以防止在 未知 状态下运行。

  对于拥有 GICv3 中断控制器并以 v3 模式运行的系统：
  - 如果 EL3 存在：
    ICC_SRE_EL3.Enable (位 3) 必须初始化为 0b1。
    ICC_SRE_EL3.SRE (位 0) 必须初始化为 0b1。
  - 若内核运行在 EL1：
    ICC_SRE_EL2.Enable (位 3) 必须初始化为 0b1。
    ICC_SRE_EL2.SRE (位 0) 必须初始化为 0b1。
  - 设备树（DT）或 ACPI 表必须描述一个 GICv3 中断控制器。

  对于拥有 GICv3 中断控制器并以兼容（v2）模式运行的系统：
  - 如果 EL3 存在：
    ICC_SRE_EL3.SRE (位 0) 必须初始化为 0b0。
  - 若内核运行在 EL1：
    ICC_SRE_EL2.SRE (位 0) 必须初始化为 0b0。
  - 设备树（DT）或 ACPI 表必须描述一个 GICv2 中断控制器。

以上对于 CPU 模式、高速缓存、MMU、架构计时器、一致性、系统寄存器的
必要条件描述适用于所有 CPU。所有 CPU 必须在同一异常级别跳入内核。

引导装载程序必须在每个 CPU 处于以下状态时跳入内核入口：

- 主 CPU 必须直接跳入内核映像的第一条指令。通过此 CPU 传递的设备树
  数据块必须在每个 CPU 节点中包含一个 ‘enable-method’ 属性，所
  支持的 enable-method 请见下文。

  引导装载程序必须生成这些设备树属性，并在跳入内核入口之前将其插入
  数据块。

- enable-method 为 “spin-table” 的 CPU 必须在它们的 CPU
  节点中包含一个 ‘cpu-release-addr’ 属性。这个属性标识了一个
  64 位自然对齐且初始化为零的内存位置。

  这些 CPU 必须在内存保留区（通过设备树中的 /memreserve/ 域传递
  给内核）中自旋于内核之外，轮询它们的 cpu-release-addr 位置（必须
  包含在保留区中）。可通过插入 wfe 指令来降低忙循环开销，而主 CPU 将
  发出 sev 指令。当对 cpu-release-addr 所指位置的读取操作返回非零值
  时，CPU 必须跳入此值所指向的地址。此值为一个单独的 64 位小端值，
  因此 CPU 须在跳转前将所读取的值转换为其本身的端模式。

- enable-method 为 “psci” 的 CPU 保持在内核外（比如，在
  memory 节点中描述为内核空间的内存区外，或在通过设备树 /memreserve/
  域中描述为内核保留区的空间中）。内核将会发起在 ARM 文档（编号
  ARM DEN 0022A：用于 ARM 上的电源状态协调接口系统软件）中描述的
  CPU_ON 调用来将 CPU 带入内核。

  *译者注: ARM DEN 0022A 已更新到 ARM DEN 0022C。

  设备树必须包含一个 ‘psci’ 节点，请参考以下文档：
  Documentation/devicetree/bindings/arm/psci.yaml


- 辅助 CPU 通用寄存器设置
  x0 = 0 (保留，将来可能使用)
  x1 = 0 (保留，将来可能使用)
  x2 = 0 (保留，将来可能使用)
  x3 = 0 (保留，将来可能使用)
```
内存map：

### 1.2 汇编时代的内存初始化

由1.1可知，bootloader跳转的第一条指令即是image header的code1和code2，这两条指令是什么？这可以从linux的arch/arm64/kernel/vmlinux.lds文件开始探寻【注：这份文件描述了整个image的布局】。
<center><img src="./img/1-2-1vmlinux首地址.png" alt="vmlinux首地址" width=800/></center>
这里可以看到首个符号是_text，虚拟地址是0xffff_8000_0800_0000。全局搜索_text，发现_text是arch/arm64/kernel/head.S文件中的符号。
```
*
 * Kernel startup entry point.
 * ---------------------------
 *
 * The requirements are:
 *   MMU = off, D-cache = off, I-cache = on or off,
 *   x0 = physical address to the FDT blob.
 *
 * Note that the callee-saved registers are used for storing variables
 * that are useful before the MMU is enabled. The allocations are described
 * in the entry routines.
 */
	__HEAD
	/*
	 * DO NOT MODIFY. Image header expected by Linux boot-loaders.
	 */
	efi_signature_nop			// special NOP to identity as PE/COFF executable
	b	primary_entry			// branch to kernel start, magic
	.quad	0				// Image load offset from start of RAM, little-endian
	le64sym	_kernel_size_le			// Effective size of kernel image, little-endian
	le64sym	_kernel_flags_le		// Informative flags, little-endian
	.quad	0				// reserved
	.quad	0				// reserved
	.quad	0				// reserved
	.ascii	ARM64_IMAGE_MAGIC		// Magic number
	.long	.Lpe_header_offset		// Offset to the PE header.
```
从其开头的代码可以判定这就是1.1中与bootloader约定好的image header。直接dump image开头，并使用hex2asm工具将其转为汇编代码
<center><img src="./img/1-2-2image-header.png" alt="image-header" width=800/></center>
<center><img src="./img/1-2-3-hex2asm.png" alt="image-header汇编" width=800/></center>
可以确认kernel就是在这里启动的。实验的image没有使用efi，所以efi_signature_nop不会执行，直接跳转到primary_entry。
```
	/*
	 * The following callee saved general purpose registers are used on the
	 * primary lowlevel boot path:
	 *
	 *  Register   Scope                      Purpose
	 *  x20        primary_entry() .. __primary_switch()    CPU boot mode
	 *  x21        primary_entry() .. start_kernel()        FDT pointer passed at boot in x0
	 *  x22        create_idmap() .. start_kernel()         ID map VA of the DT blob
	 *  x23        primary_entry() .. start_kernel()        physical misalignment/KASLR offset
	 *  x24        __primary_switch()                       linear map KASLR seed
	 *  x25        primary_entry() .. start_kernel()        supported VA size
	 *  x28        create_idmap()                           callee preserved temp register
	 */
SYM_CODE_START(primary_entry)
	/* 转存启动参数 */
	bl	preserve_boot_args
  /* 配置启动cpu的EL等级 */
	bl	init_kernel_el			// w0=cpu_boot_mode
	mov	x20, x0
  /* 创建恒等映射 */
	bl	create_idmap

	/*
	 * The following calls CPU setup code, see arch/arm64/mm/proc.S for
	 * details.
	 * On return, the CPU will be ready for the MMU to be turned on and
	 * the TCR will have been set.
	 */
#if VA_BITS > 48
	mrs_s	x0, SYS_ID_AA64MMFR2_EL1
	tst	x0, #0xf << ID_AA64MMFR2_EL1_VARange_SHIFT
	mov	x0, #VA_BITS
	mov	x25, #VA_BITS_MIN
	csel	x25, x25, x0, eq
	mov	x0, x25
#endif
	/* 配置CPU */
	bl	__cpu_setup			// initialise processor
	b	__primary_switch
SYM_CODE_END(primary_entry)
```
这里重点看下 create_idmap，它创建了一个恒等映射，也就是物理地址和虚拟地址相同的映射。由于进内核时MMU是关闭，这里执行的代码是物理地址，如果没有一个缓冲地带，直接把页表配置成线性映射，那在开启MMU后，PC会直接跑飞。例如当前开启MMU指令的PC为0x0041_0000，那PC自加后为0x0041_0004此时MMU的页表并没有映射这一地址，执行便会出错。
```
SYM_FUNC_START_LOCAL(create_idmap)
	mov	x28, lr
	/*
	 * The ID map carries a 1:1 mapping of the physical address range
	 * covered by the loaded image, which could be anywhere in DRAM. This
	 * means that the required size of the VA (== PA) space is decided at
	 * boot time, and could be more than the configured size of the VA
	 * space for ordinary kernel and user space mappings.
	 *
	 * There are three cases to consider here:
	 * - 39 <= VA_BITS < 48, and the ID map needs up to 48 VA bits to cover
	 *   the placement of the image. In this case, we configure one extra
	 *   level of translation on the fly for the ID map only. (This case
	 *   also covers 42-bit VA/52-bit PA on 64k pages).
	 *
	 * - VA_BITS == 48, and the ID map needs more than 48 VA bits. This can
	 *   only happen when using 64k pages, in which case we need to extend
	 *   the root level table rather than add a level. Note that we can
	 *   treat this case as 'always extended' as long as we take care not
	 *   to program an unsupported T0SZ value into the TCR register.
	 *
	 * - Combinations that would require two additional levels of
	 *   translation are not supported, e.g., VA_BITS==36 on 16k pages, or
	 *   VA_BITS==39/4k pages with 5-level paging, where the input address
	 *   requires more than 47 or 48 bits, respectively.
	 */
#if (VA_BITS < 48)
#define IDMAP_PGD_ORDER	(VA_BITS - PGDIR_SHIFT)
#define EXTRA_SHIFT	(PGDIR_SHIFT + PAGE_SHIFT - 3)

	/*
	 * If VA_BITS < 48, we have to configure an additional table level.
	 * First, we have to verify our assumption that the current value of
	 * VA_BITS was chosen such that all translation levels are fully
	 * utilised, and that lowering T0SZ will always result in an additional
	 * translation level to be configured.
	 */
#if VA_BITS != EXTRA_SHIFT
#error "Mismatch between VA_BITS and page size/number of translation levels"
#endif
#else
#define IDMAP_PGD_ORDER	(PHYS_MASK_SHIFT - PGDIR_SHIFT)
#define EXTRA_SHIFT
	/*
	 * If VA_BITS == 48, we don't have to configure an additional
	 * translation level, but the top-level table has more entries.
	 */
#endif
	adrp	x0, init_idmap_pg_dir
	adrp	x3, _text
	adrp	x6, _end + MAX_FDT_SIZE + SWAPPER_BLOCK_SIZE
	mov	x7, SWAPPER_RX_MMUFLAGS

	/* .macro map_memory, tbl, rtbl, vstart, vend, flags, phys, order, istart, iend, tmp, count, sv, extra_shift */
	map_memory x0, x1, x3, x6, x7, x3, IDMAP_PGD_ORDER, x10, x11, x12, x13, x14, EXTRA_SHIFT

	/* Remap the kernel page tables r/w in the ID map */
	adrp	x1, _text
	adrp	x2, init_pg_dir
	adrp	x3, init_pg_end
	bic	x4, x2, #SWAPPER_BLOCK_SIZE - 1
	mov	x5, SWAPPER_RW_MMUFLAGS
	mov	x6, #SWAPPER_BLOCK_SHIFT
	/*
 	* Remap a subregion created with the map_memory macro with modified attributes
 	* or output address. The entire remapped region must have been covered in the
 	* invocation of map_memory.
 	*
 	* x0: last level table address (returned in first argument to map_memory)
 	* x1: start VA of the existing mapping
 	* x2: start VA of the region to update
 	* x3: end VA of the region to update (exclusive)
 	* x4: start PA associated with the region to update
 	* x5: attributes to set on the updated region
 	* x6: order of the last level mappings
 	*/
	bl	remap_region

	/* Remap the FDT after the kernel image */
	adrp	x1, _text
	adrp	x22, _end + SWAPPER_BLOCK_SIZE
	bic	x2, x22, #SWAPPER_BLOCK_SIZE - 1
	bfi	x22, x21, #0, #SWAPPER_BLOCK_SHIFT		// remapped FDT address
	add	x3, x2, #MAX_FDT_SIZE + SWAPPER_BLOCK_SIZE
	bic	x4, x21, #SWAPPER_BLOCK_SIZE - 1
	mov	x5, SWAPPER_RW_MMUFLAGS
	mov	x6, #SWAPPER_BLOCK_SHIFT
	bl	remap_region

	/*
	 * Since the page tables have been populated with non-cacheable
	 * accesses (MMU disabled), invalidate those tables again to
	 * remove any speculatively loaded cache lines.
	 */
	dmb	sy

	adrp	x0, init_idmap_pg_dir
	adrp	x1, init_idmap_pg_end
	bl	dcache_inval_poc
	ret	x28
SYM_FUNC_END(create_idmap)
```
create_idmap共做了三件事：
1. 创建整个image的恒等映射
2. 修改init_pg_dir区域的属性，使其可读写
3. 修改FDT区域的属性，使其可读写

这里由个值得探讨的指令adrp。adrp的原理是基于当前PC地址计算特定符号所在运行时的页面基址（页大小为4K）。例如create_idmap中的`adrp	x0, init_idmap_pg_dir`，该指令获取的是init_idmap_pg_dir这个符号运行时所在的物理页面地址，其实现原理如下：
```
step 1: 编译时计算init_idmap_pg_dir在bin中和“adrp	x0, init_idmap_pg_dir”这条指令所在页面位置的相对偏移量，并把这个相对偏移量存储在指令的立即数中
step 2: 运行时，adrp指令执行时，首先获取当前PC地址，算出PC所在的页号，然后加上存在立即数中的offset，得到init_idmap_pg_dir所在的4K页面基址
```
事实上，adrp指令获取页面基址后，可以再加上符号的业内偏移量得到符号的绝对物理地址，kernel将这一操作包装成了adr_l
```
/*
 * @dst: destination register (64 bit wide)
 * @sym: name of the symbol
 */
.macro	adr_l, dst, sym
adrp	\dst, \sym
add	\dst, \dst, :lo12:\sym
.endm
```
建立恒等映射后，内核便可以启用MMU。这一步在__primary_switch中完成
```
SYM_FUNC_START_LOCAL(__primary_switch)
	adrp	x1, reserved_pg_dir
	adrp	x2, init_idmap_pg_dir
	bl	__enable_mmu
#ifdef CONFIG_RELOCATABLE
	adrp	x23, KERNEL_START
	and	x23, x23, MIN_KIMG_ALIGN - 1
#ifdef CONFIG_RANDOMIZE_BASE
	mov	x0, x22
	adrp	x1, init_pg_end
	mov	sp, x1
	mov	x29, xzr
	bl	__pi_kaslr_early_init
	and	x24, x0, #SZ_2M - 1		// capture memstart offset seed
	bic	x0, x0, #SZ_2M - 1
	orr	x23, x23, x0			// record kernel offset
#endif
#endif
	bl	clear_page_tables
	bl	create_kernel_mapping

	adrp	x1, init_pg_dir
	load_ttbr1 x1, x1, x2
#ifdef CONFIG_RELOCATABLE
	bl	__relocate_kernel
#endif
	ldr	x8, =__primary_switched
	adrp	x0, KERNEL_START		// __pa(KERNEL_START)
	br	x8
SYM_FUNC_END(__primary_switch)

```
查看__enable_mmu的实现，其最重要的工作时将init_idmap_pg_dir这个页表基址配置到TTBR0_EL1寄存器中，启用MMU
```
SYM_FUNC_START(__enable_mmu)
	mrs	x3, ID_AA64MMFR0_EL1
	ubfx	x3, x3, #ID_AA64MMFR0_EL1_TGRAN_SHIFT, 4
	cmp     x3, #ID_AA64MMFR0_EL1_TGRAN_SUPPORTED_MIN
	b.lt    __no_granule_support
	cmp     x3, #ID_AA64MMFR0_EL1_TGRAN_SUPPORTED_MAX
	b.gt    __no_granule_support
	phys_to_ttbr x2, x2
	msr	ttbr0_el1, x2			// load TTBR0 TTBR0_EL1负责VA高位为0的地址转换，TTBR1_EL1负责VA高位为1的地址转换
	load_ttbr1 x1, x1, x3

	set_sctlr_el1	x0

	ret
SYM_FUNC_END(__enable_mmu)
```
这里需要补充，arm64 TTBR0_EL1负责转换的是VA高位为0的地址，TTBR1_EL1负责转换的是VA高位为1的地址。由于image加载的物理地址不能是0xffff开头，所以恒等映射要使用TTBR0_EL1。

MMU开启后，会再初始化一份页表init_pg_dir，这份页表会把image映射到虚拟地址空间，这样内核就可以通过VA访问image中的数据了。从vmlinux.lds重可知image的虚拟地址KIMAGE_VADDR，也就是符号_text是从ffff800008000000开始的，这里创建的虚拟地址映射表会被加载到TTBR1_EL1中，也就是一下这两句
```
adrp	x1, init_pg_dir
load_ttbr1 x1, x1, x2
```
接下来就是调用__primary_switched，这里需要注意，`br x8`这条指令之后，PC指针就使用的是虚拟地址了，而不再是物理地址。
```
/*
 * The following fragment of code is executed with the MMU enabled.
 *
 *   x0 = __pa(KERNEL_START)
 */
SYM_FUNC_START_LOCAL(__primary_switched)
	adr_l	x4, init_task
	init_cpu_task x4, x5, x6

	adr_l	x8, vectors			// load VBAR_EL1 with virtual
	msr	vbar_el1, x8			// vector table address
	isb

	stp	x29, x30, [sp, #-16]!
	mov	x29, sp

	str_l	x21, __fdt_pointer, x5		// Save FDT pointer

	ldr_l	x4, kimage_vaddr		// Save the offset between
	sub	x4, x4, x0			// the kernel virtual and
	str_l	x4, kimage_voffset, x5		// physical mappings

	mov	x0, x20
	bl	set_cpu_boot_mode_flag

	// Clear BSS
	adr_l	x0, __bss_start
	mov	x1, xzr
	adr_l	x2, __bss_stop
	sub	x2, x2, x0
	bl	__pi_memset
	dsb	ishst				// Make zero page visible to PTW

#if VA_BITS > 48
	adr_l	x8, vabits_actual		// Set this early so KASAN early init
	str	x25, [x8]			// ... observes the correct value
	dc	civac, x8			// Make visible to booting secondaries
#endif

#ifdef CONFIG_RANDOMIZE_BASE
	adrp	x5, memstart_offset_seed	// Save KASLR linear map seed
	strh	w24, [x5, :lo12:memstart_offset_seed]
#endif
#if defined(CONFIG_KASAN_GENERIC) || defined(CONFIG_KASAN_SW_TAGS)
	bl	kasan_early_init
#endif
	mov	x0, x21				// pass FDT address in x0
	bl	early_fdt_map			// Try mapping the FDT early
	mov	x0, x20				// pass the full boot status
	bl	init_feature_override		// Parse cpu feature overrides
#ifdef CONFIG_UNWIND_PATCH_PAC_INTO_SCS
	bl	scs_patch_vmlinux
#endif
	mov	x0, x20
	bl	finalise_el2			// Prefer VHE if possible
	ldp	x29, x30, [sp], #16
	bl	start_kernel
	ASM_BUG()
SYM_FUNC_END(__primary_switched)
```
由于已经可以通过虚拟地址访问image中的内容，__primary_switched中可以直接bl调用函数，这里做了进入C语言世界的最后几件事：
1. 初始化CPU栈帧。
2. 配置异常向量表
3. 保存FDT指针
4. 保存内核虚拟地址和物理地址的偏移到kimage_voffset
5. 设置CPU启动模式标志
6. 清除BSS段
7. map fdt
8. 解析cpu feature overrides
最终调用start_kernel进入C世界！
```
内存map：

### 1.3 start_kernel中的内存初始化

- PTE:  页表项（page table entry）
- PGD(Page Global Directory)
- PUD(Page Upper Directory)
- PMD(Page Middle Directory)
- PT(Page Table)

```
4KB 页大小的转换表查找：
+--------+--------+--------+--------+--------+--------+--------+--------+
|63    56|55    48|47    40|39    32|31    24|23    16|15     8|7      0|
+--------+--------+--------+--------+--------+--------+--------+--------+
 |                 |         |         |         |         |
 |                 |         |         |         |         v
 |                 |         |         |         |   [11:0]         【页内偏移 0-4095】
 |                 |         |         |         +-> [20:12] L3 索引【PTE index 512*4KB】
 |                 |         |         +-----------> [29:21] L2 索引【PMD index 512*2MB】
 |                 |         +---------------------> [38:30] L1 索引【PUD index 512*1GB】
 |                 +-------------------------------> [47:39] L0 索引【PGD index 512*512GB】
 +-------------------------------------------------> [63] TTBR0/1
```

#### 1.3.1 fixmap

进入C世界后，MMU已开启，若想访问除了被init_pg_dir映射外的内存，则必须为该物理地址创建页表映射，但此时内存管理模块还未就绪，因此需要hard code的去建立这些映射，这就是所谓的fix_map机制：代码中先指定一片虚拟地址，用于映射早期的物理地址。

指定的虚拟地址范围为：
```
FIXADDR_TOP ~ FIXADDR_TOP - FIXADDR_SIZE
```
这段虚拟地址每一片的用途也被预先规划好，定义在arch/arm64/include/asm/fixmap.h中：
```c
/*
 * Here we define all the compile-time 'special' virtual
 * addresses. The point is to have a constant address at
 * compile time, but to set the physical address only
 * in the boot process.
 *
 * Each enum increment in these 'compile-time allocated'
 * memory buffers is page-sized. Use set_fixmap(idx,phys)
 * to associate physical memory with a fixmap index.
 */
enum fixed_addresses {
	FIX_HOLE,

	/*
	 * Reserve a virtual window for the FDT that is 2 MB larger than the
	 * maximum supported size, and put it at the top of the fixmap region.
	 * The additional space ensures that any FDT that does not exceed
	 * MAX_FDT_SIZE can be mapped regardless of whether it crosses any
	 * 2 MB alignment boundaries.
	 *
	 * Keep this at the top so it remains 2 MB aligned.
	 */
#define FIX_FDT_SIZE		(MAX_FDT_SIZE + SZ_2M)
	FIX_FDT_END,
	FIX_FDT = FIX_FDT_END + FIX_FDT_SIZE / PAGE_SIZE - 1,

	FIX_EARLYCON_MEM_BASE,
	FIX_TEXT_POKE0,

#ifdef CONFIG_ACPI_APEI_GHES
	/* Used for GHES mapping from assorted contexts */
	FIX_APEI_GHES_IRQ,
	FIX_APEI_GHES_SEA,
#ifdef CONFIG_ARM_SDE_INTERFACE
	FIX_APEI_GHES_SDEI_NORMAL,
	FIX_APEI_GHES_SDEI_CRITICAL,
#endif
#endif /* CONFIG_ACPI_APEI_GHES */

#ifdef CONFIG_UNMAP_KERNEL_AT_EL0
#ifdef CONFIG_RELOCATABLE
	FIX_ENTRY_TRAMP_TEXT4,	/* one extra slot for the data page */
#endif
	FIX_ENTRY_TRAMP_TEXT3,
	FIX_ENTRY_TRAMP_TEXT2,
	FIX_ENTRY_TRAMP_TEXT1,
#define TRAMP_VALIAS		(__fix_to_virt(FIX_ENTRY_TRAMP_TEXT1))
#endif /* CONFIG_UNMAP_KERNEL_AT_EL0 */
	__end_of_permanent_fixed_addresses,

	/*
	 * Temporary boot-time mappings, used by early_ioremap(),
	 * before ioremap() is functional.
	 */
#define NR_FIX_BTMAPS		(SZ_256K / PAGE_SIZE)
#define FIX_BTMAPS_SLOTS	7
#define TOTAL_FIX_BTMAPS	(NR_FIX_BTMAPS * FIX_BTMAPS_SLOTS)

	FIX_BTMAP_END = __end_of_permanent_fixed_addresses,
	FIX_BTMAP_BEGIN = FIX_BTMAP_END + TOTAL_FIX_BTMAPS - 1,

	/*
	 * Used for kernel page table creation, so unmapped memory may be used
	 * for tables.
	 */
	FIX_PTE,
	FIX_PMD,
	FIX_PUD,
	FIX_PGD,

	__end_of_fixed_addresses
};
```
这个枚举数据类型中的每个index单位为4KB。这段虚拟地址被分为了两段，index 0 ~ __end_of_permanent_fixed_addresses为持久映射段，index __end_of_permanent_fixed_addresses ~ __end_of_fixed_addresses为临时映射段。持久化的映射段在内核启动过程中不会改变，而临时映射段则会在内核启动过程中动态修改。

关于fixmap的api主要定义在arch/arm64/include/asm/early-fixmap.h和kernel/include/asm-generic/fixmap.h中
```c
/* 初始化fixmap */
void __init early_fixmap_init(void);
/* 输入页面fixmap区域页面idx，返回对应的虚拟地址 */
static __always_inline unsigned long fix_to_virt(const unsigned int idx)
/* 输入虚拟地址，返回对应的fixmap区域页面idx */
static inline unsigned long virt_to_fix(const unsigned long vaddr)
/* 映射phys地址到fixmap指定idx的虚拟地址，按4K粒度映射 */ 
set_fixmap(idx, phys)
set_fixmap_nocache(idx, phys)
set_fixmap_io(idx, phys)
/* 带offset映射会返回映射后，对应子节的虚拟地址 */
set_fixmap(idx, phys)
set_fixmap_nocache(idx, phys)
set_fixmap_io(idx, phys)
/* 清除映射 */
clear_fixmap(idx)
```
fixmap初始化调用路径为：
```
start_kernel()
	setup_arch()
		early_fixmap_init()
```
这里初始化的作用就是把fixmap预先定义好的bmp_pud/bmp_pmd/bmp_pte填入到init_pg_dir页表中构建出fixmap区域的页表，注意此时bmp_pte是未填充状态，使用时会进行填充。

#### 1.3.2 从dtb中获取物理内存情况

#### 1.3.1 start_kernel拾遗
- init_mm的初始值：
<center><img src="./img/1-3-1-initmm的初始值.png" alt="init_mm的初始值" width=800/></center>
- set_task_stack_end_magic: 每个task的栈帧中，栈底有一个magic number，STACK_END_MAGIC用来检测栈溢出
- init_vmlinux_build_id：每个linux kernel都会由一个build id，用来唯一标识一个kernel，这个build id在编译时由编译器生成，在内核启动时，会把这个build id保存到内核中, panic分析vmcore时可以根据build id来找对应的vmlinux。
- read_sysreg: 读取系统寄存器
- dtb开头有magic值0xd00dfeed，可以在内存中找这个值来推断dtb的位置



## Reference

<a href="/sources/articles/memory/Linux内存基础/refs/bonwick_slab.pdf" download="The Slab Allocator: An Object-Caching Kernel Memory Allocator">The Slab Allocator: An Object-Caching Kernel Memory Allocator</a>
