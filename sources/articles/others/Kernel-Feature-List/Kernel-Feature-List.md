---
layout: default
title: Kernel Feature List
categories: others
author: Nrush
create_time: 2023-05-11 00:10:33
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

## mm

- Kernel Same-page Merging (KSM): 
- Data Access Monitor (DAMON):
- Huge Page:
- Transparent Huge Page:
- ZRAM: 
- NUMA:
- pagemap:
- idle page tracking:
- soft-dirty:
- userfault:
- slab debug
- kasan
- kfence

## sched