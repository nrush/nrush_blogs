---
layout: default
title: CPU Cache漫谈
categories: architecture
author: Nrush
create_time: 2023-06-18 22:02:30
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

## 1 处理器为什么需要Cache？

性能是计算机永恒的主题之一，在如何让处理器跑的更快的道路上，现代处理器已使用了非常多的方法，如高主频、多级流水、多发射、乱序执行等等，这些方法让现代处理器拥有非常高的数据处理速度，然而相比处理器性能的提升，数据存储器件性能（主要指DRAM）的提升相对却较为缓慢，从1980年到2010年处理器的性能提升了10000倍，而数据存储器的性能只提升了10倍不到。这使得数据读写速度成为制约处理器性能的重要因素之一，处理器需要在等待数据读写上花费大量时间。
<center>
<table width="90%">
    <tbody align=center>
        <tr>
            <td><img src="/sources/articles/architecture/CPU-Cache漫谈/img/cpu-and-mem-performance.png" width=500></td>
            <td><img src="/sources/articles/architecture/CPU-Cache漫谈/img/memory-hierarchy.svg" width=400></td>
        </tr>
        <tr>
            <td>（1）1980-2010 处理器及存储器性能趋势图</td>
            <td>（2）一台家PC典型存储层级</td>
        </tr>
    </tbody>
</table>
</center>
就目前而言，存储器件读写速度越高，价格相应更贵，以高速存储器件SRAM和主流DDR对比，相同容量的SRAM的价格是DDR的数倍以上。参考下图，SRAM存储一个bit需要用掉6个晶体管，而DDR只需要一个，DDR的成本优势不言自明。
<center>
<table width="90%">
    <tbody align=center>
        <tr>
            <td><img src="/sources/articles/architecture/CPU-Cache漫谈/img/SRAM.jpg" width=350></td>
            <td><img src="/sources/articles/architecture/CPU-Cache漫谈/img/DRAM.svg" width=350></td>
        </tr>
        <tr>
            <td>(1) SRAM 1 bit</td>
            <td>(2) DRAM 1 bit</td>
        </tr>
    </tbody>
</table>
</center>
世上总不缺有钱人，价格并不是限制高速存储被用作主存的唯一因素，功耗和容量（集成度）也极大限制了高速存储用于主存的可能。还是以SRAM和DDR做对比，SRAM存储一个bit需要6个晶体管，而DDR仅需要一个，虽不严谨，但一般而言，更多的器件意味着更高的功耗。在大量读写操作下，其动态功耗也更高。存储单个bit需要更多的晶体管也意味着集成度的下降，单从晶体管数量考虑，在使用相同的晶圆面积的前提下，DDR的容量是SRAM的6倍以上。因此，从价格、功耗和容量等角度考虑，目前直接使用高速存储器件作为主存以实现处理器读写数据的性能提升是一件不太现实的事。

在无法直接使用高速存储器作为计算机系统的主存的情况下，如何提高处理器的处理效率呢？当上帝关上了这扇门，一定会为你打开一扇窗。程序的局部性原理：在一段时间内，一个程序仅会执行程序的某一部份，其访存的行为集中在某一段地址内。进一步，程序的局部性原理可以分为时间局部性和空间局部性。
```
时间局部性：被执行的指令和被访问的内存极有可能在短时间内被重复执行和访问。例：for循环代码的执行。
空间局部性：被执行的指令和被访问的内存附近的指令和内存，在短时间很有可能被访问。例：数组数据的处理。
```
程序的局部性原理意味，即使只把一小段正在运行的“部分程序”所需要的指令和数据放到高速存储器件中便能有效的提高运行数据。这就好比一个需要手工焊接电路的工程师，如果每个元件都从器件柜上单独去找，工作效率会非常低，但把常用的元件放到随手可得的器件盒中，便能极大减少工程师在器件柜和工作台间奔走的时间。对于处理器而言，这个“随手可得的器件盒”就是大名鼎鼎的Lx Cache！

以下是AIDA64采集的一张实测数据图，假设L1 Cache中所有的读操作都直接转换成从Memory直接读取，那直观上读内存相关程序代码的性能将会劣化约

$$
\frac{2306.8 \times 66.1}{2306.8 \times 1 + 1357.9 \times 3 + 891.4 \times 15.7 + 92.4 \times 66.1} \approx 5.75
$$

这就是Cache的巨大威力!此外，这份数据也很好的印证了程序的局部性原理，纯Cache的支撑的访问量是真实Memory访问量的23倍有余，能满足处理器对数据的大部分访问请求。
<center><img src="/sources/articles/architecture/CPU-Cache漫谈/img/mem-reference-data.webp" alt="mem-reference-data" width=600/></center>

总结而言，处理器Cache的必要性来源于如下几点：
- 处理器性能和主存性能之间的巨大差异。
- 高速存储器件的高价格、高功耗和低容量限制。
- 程序执行访存行为具有的空间、时间局部性。

使用Cache的收益：
- 获得性能、数据容量、功耗和价格的折中。在保证存储器容量大，功耗低，成本低的前提下大幅提升计算机系统性能。

## 2 Cache的基本原理

Cache本质上需要解决的问题是如何尽可能避免处理器直接访问速度较慢主存。一般而言现代计算机的CPU cache结构都是多级的且涉及到多CPU，但为了便于研究Cache的基本原理，本章只讨论单CPU、单级Cache的情形。

<center>
<table width="90%">
    <tbody align=center>
        <tr>
            <td><img src="/sources/articles/architecture/CPU-Cache漫谈/img/cache-basic.svg" width=200></td>
        </tr>
        <tr>
            <td>单CPU、单级cache结构</td>
        </tr>
    </tbody>
</table>
</center>

要说清Cache的基本原理，需要回答如下的几个问题

1. Cache中的资源要如何组织与主存的内存资源相对应？
2. 主存中的数据何时被添加到Cache中？
3. 如何判断处理器需要的数据是否在Cache中？
4. Cache被填满后该怎样选择需要替换的数据？
5. 当处理器对Cache中的数据做写操作后，需要如何处理？

### 2.1 Q1: Cache中的资源要如何组织与主存的内存资源相对应？

从程序的空间局部性原理出发，Cache和主存间交换数据的最小大小不应是字节，当出现Cache miss（CPU所需要的数据不在Cache中，需要从主存中加载）时应当将CPU当前访问地址附近的数据一并加载到Cache中，以备后续的访问。由此启发，可以将Cache和主存分割成大小相同的连续内存块，Cache每次从主存中读写的最小单位就是这样一个连续内存块，这个块就是所谓的Cache Line，Cache-line的Size一般为16、32或64等等。后文为了简化，统一将1个Cache-line称为1个block。

出于成本、功耗、集成度等因素的考虑，Cache的容量是远小于主存的，这点决定了Cache中block到主存中block的映射关系只会是一对多的映射，也就是说Cache中的每个block都与主存中的多个block相对应。但从主存block角度看，这种映射关系将并非是唯一的，主存block可以固定映射到cache的特定block，也可以映射到其中的某些block，还可以是映射到cache中的任意block，采用这三种映射方式的Cache分别被称为直接映射高速缓存(direct-mapped cache)、组相联高速缓存(set associative cache)和全相联高速缓存(fully associative cache)。实际上这三种映射都是组相联(set associative，这里的set指的是Cache中的一组block)的设计策略，直接映射高速缓存相当于每个set只有一个block，全相联高速缓存相当于整个Cache只有一个set。如果每个set中有n个block，则这种组相联会被称为n路组相联(n-ways set associative)，主存中的block可以被存放在对应set的任意路中。假设通过按下式求余的决定主存的block被放到哪个set中

$$
    (Set \; index) = (Block \; address) \; \mathbf{MOD} \; (Number \; of \; sets \; in \; cache)
$$
则通过下图可以比较直观的理解block、set和way以及它们之间的关系。

<center>
<table width="90%">
    <tbody align=center>
        <tr>
            <td><img src="/sources/articles/architecture/CPU-Cache漫谈/img/set-associative.svg" width=800></td>
        </tr>
        <tr>
            <td>不同组相联策略的对比</td>
        </tr>
    </tbody>
</table>
</center>

事实上Cache的block大小、set数、way数配置都会对Cache的性能产生影响，不过这是单独要讨论的内容，这里不进行讨论。

### 2.2 Q2：主存中的数据何时被添加到Cache中？

这个问题的答案比较直观，当CPU需要的数据不在缓存中时主存中的数据需要被加载。不妨再深入一些，哪些情况会导致CPU需要的数据会不在Cache中（也就是cache miss）呢？对于cache miss的原因，大抵可以分为三类：

```
1. 必然的cache miss：CPU在第一次请求某个数据时，这个数据必然不可能在cache中，因此必然会经历从主存加载到cache的这一过程。这种cache miss具有必然性，无论怎样优化cache机制、增加cache size，这类cache miss都是必须的。

2. 由于cache size不足导致的cache miss：cache的容量是有限的，就像一辆座位数固定的大巴，满员后必须有人下车，后来的人才有座位。当cache size满足不了程序运行时所需要访问的数据，那必然会有block被踢出，当程序再次想要访问这份被踢出的数据时，对应的block需要重新被加载，因此会照成cache miss。

3. 由于数据冲突导致的cache miss：如果cache的组织方式不是全联接的，cache中的一个block必然会与多个主存中的多个block相对应，这意味着会出现多个主存的block被映射到同一个set，进而发生冲突，必须踢出先加载的block来加载目前需要的block，当被踢出的block再次被需要时，这会产生一次cache miss
```

cache miss的优化是CPU性能优化的重要一环，了解cache miss产生的原因能帮助寻找优化手段和方法，不过这是后面的内容了，这里也不做过多的讨论。

### 2.3 Q3 如何判断处理器需要的数据是否在Cache中？

如何高效的判断CPU所需的数据是否在Cache中是Cache设计的重要内容。先来看看CPU侧提供了什么信息？一般而言CPU发出的访存指令是使用的是虚拟地址，这个虚拟地址经过TLB（Translation Lookaside Buffer）或者MMU后会变为物理地址，也就是说Cache获得的信息有两个，物理地址和虚拟地址。下图所展示了没有缓存时CPU从主存中读取数据的工作流（写入也是类似的，这里暂且只讨论读的情况，写的问题会放到Q4中讨论）
<center>
<table width="90%">
    <tbody align=center>
        <tr>
            <td><img src="/sources/articles/architecture/CPU-Cache漫谈/img/cpu-read-without-cache.svg" width=600></td>
        </tr>
    </tbody>
</table>
</center>
图中的VA（Virtual Address）表示虚拟地址，PA（Physical Address）表示物理地址。从上图不难发现，PA和VA的获取的时间开销是不一致，VA直接从CPU拿就好，开销小，而PA需要通过TLB或者MMU的转换才能拿到开销大。此外VA、PA和主存中物理内存的映射关系也是不一样的，一个VA可以被映射到多个物理内存（注：一个VA在同一时刻只会被映射到一个物理内存，但不同物理地址可以被分时映射到同一个VA，），这被称为歧义（ambiguity）问题，同时一个物理内存也可以被映射到多个VA，这被称为别名（alias）问题。而对于PA而言，一个PA只映射到一块物理内存，一块物理内存也只映射到一个PA，PA的映射是一对一的。

<center>
<table width="90%">
    <tbody align=center>
        <tr>
            <td><img src="/sources/articles/architecture/CPU-Cache漫谈/img/VA-mem-map.svg" width=500></td>
        </tr>
        <tr>
            <td>(1) VA map</td>
        </tr>
        <tr>
            <td><img src="/sources/articles/architecture/CPU-Cache漫谈/img/PA-map.svg" width=500></td>
        </tr>
        <tr>
            <td>(2) PA map</td>
        </tr>
    </tbody>
</table>
</center>

VA与物理内存的映射关系比较复杂，不妨先从PA入手，Cache的输入是PA，输出是PA对应物理内存的数据。
<center>
<table width="90%">
    <tbody align=center>
        <tr>
            <td><img src="/sources/articles/architecture/CPU-Cache漫谈/img/PIPT-read.svg" width=750></td>
        </tr>
    </tbody>
</table>
</center>
对于一个直接映射Cache，根据Q1，需要将PA转换PA对应物理内存映射的Cache的set号以及在对应block中的offset。那不妨先把把PA分为两部分一部分用来表示set号，一部份表示block内的偏移，那就得到了一种将物理内存映射到cache block中的函数，但找到set和block偏移并不能保证找到的这个字节就是CPU需要的那个字节，一般而言Cache的size是远远小于主存大小的，这意味着主存中的多个block会被映射到同一个cache的block中，因此还需要一个唯一的标识来确认Cache中的这个block中是否的确是CPU需要的数据。PA能表示的内存空间是要大于Cache的，因此在分配一些bits给set index和offset后，还会余下一些bits，这些bits便可以用来作为标识。为了能对比PA上的标识，Cache在从主存中加载PA对应的物理内存数据时，block还需要额外存储下PA的标识域，表示当前这个block中存储的数据是和这个PA唯一对应的，如果映射到这个block的另一个PA内存被加载，那么Cache中该block存储的标识也会相应变化，之前的PA再次访问此block时将由于标识的不匹配而被认为时cache miss进而重新加载数据，保证数据的正确性。如果这个Cache是只读的且主存上的数据也是只读的，那么上面的机制已经是完备的，但现实情况中是会有写行为的，对于单CPU，可能有绕过Cache的写行为；对于多CPU或多IP，可能出现多CPU、多IP读写同一片内存的行为，这些行为都可能导致Cache中的数据与主存中的数据不一样的情况（一致性问题）。为了应对这种一致性问题，可以在block存储的信息中添加一个bit表示该行数据是否有效，数据无效时按Cache miss处理即可。此外为解决cache读操作导致的cache 主存一致性问题，block中还需要1个bit来标识这个block是否被写过，当然这不是Q3讨论的重点，关于写操作的细节会在Q4中讨论，另外缓存一致性也是一个大主题，这会单独用一章来讨论。

Talk is cheap！现在以一个实例来看看Cache是如何判断CPU发出的一个PA是否是命中的。考虑一个寻址能力为32位的CPU，其有一级64KB，cacheline size（block size）为64的直接相联Cache，当CPU需要访问的物理地址为0x12345678时,Cache如何判断0x12345678是否命中呢？先来看看PA中offset、set index、tags分别是是多少位

$$
    cacheline size = 2^6 \rightarrow offset: \; 6bits\\
    set number = \frac{64 \times 1024}{2^6} = 2^10 \rightarrow set index: \; 10bits\\
    tags bits number = 32 - 6 - 10 = 16 \rightarrow tags: \; 16bits\\
$$

由此0x12345678可以依次被分割成tag（16bit）：set index（10bits）：offset（6bits），对应的offset、set index、tags值分别为0x38、0x159、0x1234。首先根据set index可以直接找到0x12345678对应的cache中的block，将这个block tag域与0x159对比，同时校验block中和validated位，当tag相等且validated标记此block为有效时，判定为命中，返回此block中对应offset位置的byte，否者从主存中加载0x12345678所在的主存block到cache中，然后再返回对应offset位置的byte。
<center>
<table width="90%">
    <tbody align=center>
        <tr>
            <td><img src="/sources/articles/architecture/CPU-Cache漫谈/img/cache-hit-1.svg" width=700></td>
        </tr>
    </tbody>
</table>
</center>

上述这套PA为输入的cache机制被称为PIPT（Physical Index Physical Tag）。人对效率的追求是无穷无尽的，正如农场主希望奶牛每天挤出更多的奶，老板希望员工在一周的工作日里干完跟多的活，Cache设计者希望Cache能够在最短的时间内给出CPU所需要的数据。还记得前文提到的VA和PA获取的时间代价差异吗？由于PA需要由VA经过TLB或MMU获得，其获取代价是要高于的VA的，因此使用VA替代PA是提升Cache性能的明显优化点。鱼和熊掌不可兼得，性能的提升必定有代价，由于前文提到的歧义和别名问题，直接将VA带入到PIPT的框架中，Cache是无法工作的。
<center>
<table width="90%">
    <tbody align=center>
        <tr>
            <td><img src="/sources/articles/architecture/CPU-Cache漫谈/img/VIVT-read.svg" width=750></td>
        </tr>
    </tbody>
</table>
</center>
先来看看歧义会带来什么问题，假设linux系统中A进程在VA为0x12345678，对应PA为0xAAAAAAAA的内存上存放了数据0xAA，且该内存数据已被作为有效数据缓存在cache中，此时如果系统切入进程B执行，而B进程被切出前在VA 0x12345678对应的PA 0xBBBBBBBB上存储的数据为0xBB，此刻B进程再次访问VA 0x123456时，VIVT Cache根据Index校验Tag将会判定为hit，并返回0xAA，这是一个错误数据。如何解决呢？A进程在切出时，同时失效掉所有cache。当然失效所有cache是开销十分大，为cache的tag添加一个校验PID的域也是一个不错的选择。

别名会有什么问题呢？假设PA 0xAAAAAAAA同时被映射到某个进程的VA 0x12345678和0x12345679，那么对于VIVT的Cache而言，由于两者的Tag和Set index是一样的，CPU在读VA 0x12345678和0x12345679时，PA 0xAAAAAAAA对应的block将会被同时被缓存到0x12345678和0x12345679对应的cacheline中。这似乎没什么大问题，VIVT能找到正确的数据，但这造成宝贵cache空间的浪费，同一个物理地址的内容，在cache中缓存多次。这将会提高cache miss率，进而影响cpu的性能。

VIVT的方式理论上可以带来性能的提升，但这种性能提升的代价是歧义、重名问题带来的过高的软硬件维护复杂性，从实现角度看，是否值得是值得权衡的。

退一步，海阔天空。如果无法完全使用VIVT，可以退而求其次使用VA的信息作为set index和offset，而PA的一部分作为tag。这样VA可以同时发送给TLB、MMU和cache，cache在查找set和offset时，TLB和MMU会同步将VA转换为PA，这意味相比PIPT VA->PA + Set、offset查找的串行模式，VIPT的方式性能更好。除了性能上的提升，VIPT的另一个好处是歧义和别名问题得到了解决。





## 附录：一些小实验

- Linux下获取cache的信息方法：
```sh
    cat /sys/devices/system/cpu/cpu0/cache/<index>/<info node>
```

## Reference

\[1\] John L. Hennessy, David A. Patterson: ***Computer Architecture: A Quantitative Approach***

\[2\] [Synopsys: Understanding Automotive DDR DRAM](https://www.synopsys.com/designware-ip/technical-bulletin/automotive-ddr-dram.html)
