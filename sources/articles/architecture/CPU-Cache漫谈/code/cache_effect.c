#include <stdio.h>
#include <time.h>

int table[2048][1024];

int main(int argc, char *argv[]) {
    clock_t start, stop;
    int i, j;

    start = clock();
    for (i = 0; i < 1024; i++) {
        for (j = 0; j < 2048; j++) {
            table[i][j] = i * j;
        } 
    }
    stop = clock();
    printf("way 1: %d cycles\n", stop - start);

    start = clock();
    for (i = 0; i < 1024; i++) {
        for (j = 0; j < 2048; j++) {
            table[j][i] = i * j;
        } 
    }
    stop = clock();
    printf("way 2: %d cycles\n", stop - start);
}