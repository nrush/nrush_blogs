---
layout: default
title: DDR基础
categories: architecture
author: Nrush
create_time: 2023-04-18 00:33:17
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

&emsp;&emsp;DDR全称Double Data Rate Synchronous Dynamic Random Access Memory，是当代处理器必不可少的存储器件之一。本文关于DDR介绍的核心点如下：
1. DRAM存储基本原理
2. DDR性能通识

## 1 SDRAM存储基本原理

&emsp;&emsp;DDR属于DRAM的一种，DRAM（Dynamic Random Access Memory）的核心词Dynamic是指这种存储器件需要**动态刷新**以保证存储的数据不丢失，与之相对的是SRAM（Static Random Access Memory）。这里的‘刷新’在DRAM中具体指什么呢？这就需要从DRAM的基本存储原理说起。

### 1.1 Bit存储

&emsp;&emsp;现代数字存储最基本的单位是bit，以下是DDR存储一个bit的基础电路
<center><img src="/sources/articles/architecture/DDR基础/img/cell.svg" alt="cell" width=250/></center>
这里有三个关键器件MOSFET，电容和放大器。MOSFET可以理解成一个电子开关，当G端为1时S和D端将会导通，否者处于断开状态（当然这个描述并不准确，MOSFET的导通关闭需要满足特定条件，这并非本文重点，因此不赘述）；电容是储能元件，被用来存储bit信息，当其充满电时可被认作为逻辑1，否则被看作逻辑0；放大器是一种可以将微弱电流电压信号放大到易于观测的量级的器件。

&emsp;&emsp;对于此电路，具体如何读取和写入bit呢？对于读取过程，先对column line经行预充电，电位置为Vcc/2（Vcc为系统供电电压），然后将row line置为1使S，D端连通。如果电容中有电荷且电位高于Vcc/2，那AMPS将会检测到一个微小的电压抬升$\delta u_1$，此时可以认为读到了逻辑1，该逻辑值将会被存储到锁存器中；如果电容中没有电荷或者电位小于Vcc/2，那这次导通将会对电容进行充电，进而导致column line上电压出现微小的电压下降$\delta u_2$，AMPS会检测到该电压变化，并认为读到了逻辑0，同样该值也会被存入锁存器。对于写入1，只需要将column line置为Vcc，然后row line置为1使S，D导通，对电容充电使其电位高于Vcc/2；如果要写入0，则只需要执行读取操作，不充电即可，因为无论先前电容电位是否高于Vcc/2，由于读取过程中的电荷泄露，读取完成后该值都将低于Vcc/2。从另一个角度说，对电容读取是个破坏存储的过程，无论之前存储的信息是多少，读取后都将归为0。如何维持信息bit 1不被读出操作破坏呢？只需在读取后立刻向电容写入存储在锁存器中的值即可。事实上，由于MOSFET、电容漏电的原因，如果长时间不进行写入1操作，原本处于Vcc/2以上电位的电容，会因为漏电而导致电位掉到Vcc/2以下，这将导致bit1的信息被反转成bit0。因此需要对该电路进行定期的读写操作以保证信息的不丢失，这里的定期读写就是上文提到的刷新，即D字的来源。

### 1.2 Memory Array

&emsp;&emsp;显然只能存储一个bit的系统是没有实用价值的，这节介绍如何将这些bit组织成大规模内存单元。现在将上一节中介绍bit存储单元组织成如下图的矩阵形式
<center><img src="/sources/articles/architecture/DDR基础/img/mem-array.svg" alt="cell" width=500/></center>
上图中4x4的方块矩阵中，每个小方块对应于一个bit存储电路，深色为1，浅色为0。同一行的存储电路共用row line，同一列的存储电路共用column line，那根据上一节中的读写原理，实际上如果指定操作一根row line，一根column line，其实等效确定的去操作特定的bit。因此只要给这个bit矩阵的行和列添加一行译码器（根据输入地址选择特定行）和列译码器即可指定操作任意的bit，例如上图中使用行地址0x02列地址0x01既可选中编号为[2,1]的bit进行读写操作。

### 1.3 Cell 和 Bank
这样的布局理论上已可以进行大规模数据存储，只要这个排列足够大即可。但读写操作对column line充电这一操作是比较耗时的，每次充电只能读取1bit效率比较低，如何实现一次充电操作读取多个bit呢？
<center><img src="/sources/articles/architecture/DDR基础/img/bank.svg" alt="cell" width=350/></center>
事实上，如上图将多个内存矩阵堆叠排列，所有的行列都接到同一组地址线上即可实现多bit读写（当然每列的数据锁存是独立的，只是共享列地址）。此时一组行列地址对应多个bit。堆叠的整体被称为bank，一般由4、8或16个内存矩阵进行堆叠，对应的这些bank会有4bit，8bit或16bit的数据线，通常以x4、x8和x16进行命名区分。所有memory array中相同行列的bit，被称为一个cell。

### 1.4 Rank

现代处理器的数据总线一般是64bit或者更高，但上一节中提到的bank从设计角度一般被配置成x4、x8和x16，数据总线和处理器的数据总线位宽并不对齐，此时可以将多个bank的数据总线并联以获取足够的位宽。以x8的bank为例，只需要8个bank并联，每次读取64bit时选取这些bank的相同行，不同列，便可以实现一次读取64bit数据。这样一个能够提供独立内存访问的bank组可以被称为是一个rank。多bank的排列除了能对齐数据总线位宽外，其还能极大拓展rank的内存的大小。
<center><img src="/sources/articles/architecture/DDR基础/img/rank.svg" alt="cell" width=400/></center>

### 1.5 DIMM
将一个或多个rank组织到PCB上，并用不用的片选线予以区分，便是常说的内存条，也就是DIMM（Dual In-line Memory Module）。
<center><img src="/sources/articles/architecture/DDR基础/img/DIMM.svg" alt="DIMM" width=700/></center>

### 1.6 小结

通过以上对cell、memory array、bank以及rank的介绍，基本上可以理解对DRAM原理。但这些介绍还只停留在逻辑层面，现实中的DDR远比上面描述的要复杂的多。下一章将从高性能DDR实现的角度来进一步深入介绍DDR的基础知识。

## 2 DDR性能通识

在对DDR进行选型时，有许多参数需要参考，例如功耗、内存大小、带宽、时延等，本章将针对带宽性能相关的内容进行探讨。

### 2.1 再看bit存储、memory array、bank和rank
有了上一章的基础认值，从性能角度再回顾一下DRAM的存储原理，会有不一样的发现。
从最基础的bit存储看：
1. 一个bit的在读写过程中比较耗时的是column预充电的过程。
2. column line上的功能要远比row line上的器件复杂。
3. bit需要定时刷新，不刷新或超时刷新可能导致数据失效，这意味着，对于共享一套刷新系统的bit而言，刷新率和内存大小上限应该是正相关的。

memory array的行列数是个值得讨论的参数。根据总结的bit存储的几个特点可以推测：
1. 从空间效率考虑，memory array的行数应多于列数。由于列上的功能更加复杂，相应的需要的器件也会更多，相同数量的bit的情况下，以相对更多的行和更少的列进行排布将更节省晶圆的物理面积占用。
2. 从读写性能考虑，列数应尽量多。如果一味的减少列数提高空间利用率，将会有读写性能下降的问题。假设memory array只有一列，每读取一个bit都需要对column进行一次充电，充电时间为$t$，则读取$n$bit必须要$nt$；而对于有$n$列的memory array，如果需要读取的bit恰好是在相同的row不同的col上，那可以同时$n$列进行充电读取，读取$n$bit的时间仅为$t$。

对于一个bank而言
1. cell包含的bit数越高，读写带宽越高。一个cell中包含的bit数越多，那么一次读取操作获取的bit数越多，带宽效率自然越高。
2. 多bank情况下，交叉读写不同bank，可以有效提高读写带宽。一个bank完成对一行的数据读写后，如果需要再次读取同一bank的另外一行上的数据，那必须耗费较长的时间重新打开一行，如果只有一个bank，那此时总线必须处于等待状态。假设需要读取的地址处于两个不同的bank中，那么在向其中一个bank发完第一个读指令后，此时不再需要等待第一笔数据返回，便可以向另一个bank发送读指令读数据，这样对总线的利用率是最高的。当bank多到一定量时这种轮流读的方式将形成循环，从外部看相当于不存在刷新等待。例如下图，若只有一个bank读取三次等长数据需要3个周期，若多bank交替读取，则只需要2个周期，提高了读取效率。

<center><img src="/sources/articles/architecture/DDR基础/img/mutibank.svg" alt="mutibank" width=600/></center>

上述这种交替读取的方式被称为交织（interleaving），本质上是将读写操作均衡到不同bank，具体如何实现这种对bank均衡？可以通过地址编码实现。例如，考虑4bit地址对4个2x2的bank进行均衡，则可以按下图编码实现交织，该编码保证对同一bank的连续地址访问都是在同一行上的。

<center><img src="/sources/articles/architecture/DDR基础/img/add_interleave.svg" alt="mutibank" width=350/></center>

同用提高bank数来提高带宽效率一样，多个rank也可以在一定程度上形成交叉读写的时序，以提高DRAM的带宽性能。除此之外，因为rank间的数据总线及其他硬件资源都是独立的，如果控制器支持同时读写多个rank，也就是多通道，那对于不同通道上的rank完全可以实现并行读写，理论带宽翻倍。

### 2.2 理想照进现实--Micron DDR 分析

<center><img src="/sources/articles/architecture/DDR基础/img/MDRAM.png" alt="Micron DRAM" width=900/></center>

上图是Micron DDR和SDRAM的实际框图，从图中的bank数目，bank的行列数，不难验证上一节中关于DRAM的一些分析。值得注意的是行列地址线是共用的12根地址线，加上额外的两根bank地址线一共14根，这远不能表示我们常用的4G、8G内存中所有的地址，事时上行列地址线是分时复用的，配合CAS、RAS这些控制信号，理论上可实现26bit的寻址空间，远高于14bit。地址线分时复用，一方面减小了走线所占的空间成本，另一方面也比较好的契合DRAM需要先打开行再选中列的特性。

这张图不仅画出了DRAM的存储结构，还体现出了DDR和SDRAM的区别。DDR和SDRAM的主要区别是在它的IO/interface部分，SDRAM和DDR的数据总线DQ的位宽都是4bit，但和SDRAM 4bit数据直接输入输出不同，DDR有读写缓冲出区，其位宽为8bit，是数据总线的2倍，同时注意到DDR的每个cell中存储的bit数也是数据总线的两倍，这就意味着，在上图中，DDR的一次访问可以操作8bit，而SDRAM是4bit，在黄色区域CLK同频的情况下，DDR是SDRAM带宽的2倍，这也是Double Data Rate的含义。这种一次读写为数据总线位宽两倍的操作被称为2n预取(prefetch)，对于DDR3\DDR4预取数被提高到8n，也就是一次读写操作可以处理的数据是数据总线位宽的8倍。不同于存储在bank中的数据，被预取在缓冲区的数据以极高的速度被使用DDR的master操作，可以说预取操作极高的提升了DDR带宽性能。下图比较清楚的体现了prefetch对DDR带宽性能的影响
<center><img src="/sources/articles/architecture/DDR基础/img/prefetch-1.png" alt="Micron DRAM" width=900/></center>
和预取操作相关的还有一个重要概念Brust Length，突发访问长度，简称BL。这个概念的含义是master对DDR一次访问的数据量大小，例如BL=2意味着一次访问将会向DDR颗粒连续传输2x数据总线位宽个bit，这有益于大批量的数据传输，提高数据总线的带宽利用率。BL应当大于等于预取数，当BL大于预取数时，其带宽上的收益来源于节省行打开的开销，一行打开后，将批量读取该行上的列数据。

对比上图和DDR4 x4的框图可以发现，DDR4中多了Bank Group
<center><img src="/sources/articles/architecture/DDR基础/img/MDDR4x4.png" alt="Micron DRAM" width=900/></center>
这个BnakGroup主要也是提升在不同bank间切换读写的速度，主要优化效果时，访问不同bank group中的bank的效率要高于相同bank group中的bank，具体原理在此不做深究。
<center><img src="/sources/articles/architecture/DDR基础/img/bankgroup-1.png" alt="Micron DRAM" width=800/></center>

### 2.5 DDR的性能参数

- prefetch：等于内部数据总线和数据总线的位宽倍数关系。
- burst length: 一次访问连续操作的数据量，数据量 = BL * 数据总线位宽。
- Data Rate：数据传输速率，单位Mbps。
- CLK frequency：DDR时钟频率，单位Mhz。
- tRC: 同一个bank中不同行被激活的最小时间间隔。
- CL (CAS Latency)：列选通后数据从存储单元到数据IO的时间。

tREFW（refresh window）: 所有内存器件都有一个刷新周期，这个周期内所有bank必须进行刷新操作，tREFW表示刷新周期内每个bank刷新操作的最小时间间隔。
tREFC (refresh command): 执行一个刷新命令所需的时间
tREFI (refresh interval): 发送刷新命令的时间间隔

auto-refresh: 自动刷新，由DDR控制器自动控制，不需要用户干预。
manual-refresh: 手动刷新，由用户控制，需要用户发送刷新命令。




##  3 总结

DDR作为当前设备的主流大容量主存设备，影响着产品的成本、功耗、性能等诸多方面，了解DDR基本原理和性能参数，对做好应用十分重要，本文对DDR的介绍还是相对基础和不全面的，后续将会继续充实和完善。


## 4 Reference

\[1\] [DDR5/4/3/2: How Memory Density and Speed Increased with each Generation of DDR](https://blogs.synopsys.com/vip-central/2019/02/27/ddr5-4-3-2-how-memory-density-and-speed-increased-with-each-generation-of-ddr/)

\[2\] [Micron TN-46-05: GENERAL DDR SDRAM FUNCTIONALITY](https://media-www.micron.com/-/media/client/global/documents/products/technical-note/dram/tn4605.pdf?rev=19186dcf48cc4aaeb55b6e44bfc4a51e)

\[3\] [DDR4 Tutorial Understanding the Basics](https://www.systemverilog.io/design/ddr4-basics/)

\[4\] [Micron Datasheet: DDR4 SDRAM](https://media-www.micron.com/-/media/client/global/documents/products/data-sheet/dram/ddr4/4gb_ddr4_dram.pdf?rev=a4122900efb84963a0d9207033a5a286)

\[5\] [Micron: New Features of DDR3 SDRAM](https://media-www.micron.com/-/media/client/global/documents/products/technical-note/dram/e1503e10.pdf?rev=83acba24ab8f4db7936700b3b63a620b)

\[6\] [Micron TN-40-4: DDR4 Point-to-Point Design Guide](https://media-www.micron.com/-/media/client/global/documents/products/technical-note/dram/tn4040_ddr4_point_to_point_design_guide.pdf?rev=d58bc222192d411aae066b2577a12677)





