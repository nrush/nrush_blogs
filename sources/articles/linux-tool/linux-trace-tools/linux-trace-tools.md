---
layout: default
title: linux trace tools
categories: linux-tool
author: Nrush
create_time: 2024-02-19 22:49:01
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

```
# list avaible funtions to trace. 'sched*' is a filter.
perf ftrace -F sched*

# Trace given functions using function tracer on existing process id (comma separated list)
perf ftrace -T sched* -p 1919,1920

# Trace given functions using function_graph tracer on existing thread id (comma separated list)
perf ftrace -G sched* --tid 2121,2222

# Trace all function on the command 'ls'
perf ftrace -t funtion -- ls
```

## Reference
