---
layout: default
title: gdb cheat book
categories: linux-tool
author: Nrush
create_time: 2024-01-14 17:57:28
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

info proc mappings:查看进程的内存映射
dump memory <ouputfile-path> <start_add> <end_add>: dump内存
starti: 执行第一条指令
i r: 查看寄存器
layout r: TUI寄存器视图

## Reference
