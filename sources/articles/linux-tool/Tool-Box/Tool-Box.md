---
layout: default
title:  Tool Box
categories: linux-tool
create_time: 2023-3-12 12:10:10
author: Nrush
---

<center><h1>{{page.title}}</h1></center>

This page is used to record some useful tools I used.

## Emulator
1. QEMU: A powerful emulator.
  - official website: <https://www.qemu.org>
  - github: <https://github.com/qemu/qemu>
  - documentation:  <https://qemu.readthedocs.io/en/latest/>

## Misc

1. WaveDrom:  A Free online digital timing diagram (waveform) rendering engine.
  - github: <https://github.com/wavedrom/wavedrom.github.io>
  - online: <https://wavedrom.com/editor.html>

1. \[&#9733;\]drawio: A free, open-source online diagramming tool.
  - github: <https://github.com/jgraph/drawio-desktop/releases/>
  - online: <https://app.diagrams.net/>

1. \[&#9733;\]chatgpt: A powerful AI!!!!
  - comments: strongly recommend.
  - official website: <https://chat.openai.com>

1. snakeviz: A python performance analysis tools.
  - github: <https://jiffyclub.github.io/snakeviz>

1. fbi: A test tool for frambuffer device.
  - short guide: `fbi -T <console id> <picture path>`

1. nohup: A tool can keep process runing even the console is colsed.
  - short guide: `nohup <command> &`

1. IDA (Interactive Disassembler): A software commonly used in reverse engineering for analyzing binary files.
  - official website: <https://hex-rays.com>

1. sysbench: a scriptable multi-threaded benchmark tool based on LuaJIT.
  - github: <https://github.com/akopytov/sysbench>

1. perfetto: A system profiling, app tracing and trace analysis tool.
  - official website: <https://perfetto.dev/>

1. AIDA64: A system diagnostics and information collection software for windows. It also can be used as performance testing tool or stress testing tool.
  - official website: <http://www.aida64.com.cn/>

1. FlameGraph: A powerful performance analysis tool.
  - github: <https://github.com/brendangregg/FlameGraph.git>
  - short guide: 
  ```
  perf record -F 99 -p 181 -g -- sleep 60
  perf script -i perf.data &> perf.unfold
  ./FlameGraph/stackcollapse-perf.pl perf.unfold &> perf.folded
  ./FlameGraph/flamegraph.pl perf.folded > perf.svg
  ```

1. CodeGeeX: An AI code completion tool.
  - github: <https://github.com/codechecker/codechecker>
  - short guide: search "CodeGeeX" in the vscode extension market.

1. EEMBC: An open source performance benchmarking tool.
  - official website: <https://www.eembc.org/>

1. Coremark: A simple embedded cpu benchmark.
  - github: <https://github.com/eembc/coremark>
  - short guide: git clone the repo, then run `make` in the root dir.

1. Nmap: A network scanner.
  - github: <https://github.com/nmap/nmap>
  - short guide: `nmap -v -sn 192.168.1.0/24` scan the local network.

1. enthtool: A network setting tool.
  - short guide: `enthtool <ethid>` get info about the network interface card, ethid can be got by `ifconfig`

``




