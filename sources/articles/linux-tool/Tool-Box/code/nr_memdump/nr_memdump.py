import re

# graph_info
# name
# level
# size


SVG = '''\
<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg version="1.1" width="{width}" height="{height}"
     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
     
<style type="text/css">
    .func_g:hover {{ stroke:black; stroke-width:0.5; cursor:pointer; }}
</style>
<script type="text/ecmascript">
<![CDATA[
	var details, svg;
	function init(evt) {{ 
		details = document.getElementById("details").firstChild; 
		svg = document.getElementsByTagName("svg")[0];
	}}
	function s(info) {{ details.nodeValue = "Function: " + info; }}
	function c() {{ details.nodeValue = ' '; }}
	function find_child(parent, name, attr) {{
		var children = parent.childNodes;
		for (var i=0; i<children.length;i++) {{
			if (children[i].tagName == name)
				return (attr != undefined) ? children[i].attributes[attr].value : children[i];
		}}
		return;
	}}
	function orig_save(e, attr, val) {{
		if (e.attributes["_orig_"+attr] != undefined) return;
		if (e.attributes[attr] == undefined) return;
		if (val == undefined) val = e.attributes[attr].value;
		e.setAttribute("_orig_"+attr, val);
	}}
	function orig_load(e, attr) {{
		if (e.attributes["_orig_"+attr] == undefined) return;
		e.attributes[attr].value = e.attributes["_orig_"+attr].value;
		e.removeAttribute("_orig_"+attr);
	}}
	function update_text(e) {{
		var r = find_child(e, "rect");
		var t = find_child(e, "text");
		var w = parseFloat(r.attributes["width"].value) -3;
		var txt = find_child(e, "title").textContent.replace(/\([^(]*\)/,"");
		t.attributes["x"].value = parseFloat(r.attributes["x"].value) +3;
		
		// Smaller than this size won't fit anything
		if (w < 2*12*0.59) {{
			t.textContent = "";
			return;
		}}
		
		t.textContent = txt;
		// Fit in full text width
		if (/^ *$/.test(txt) || t.getSubStringLength(0, txt.length) < w)
			return;
		
		for (var x=txt.length-2; x>0; x--) {{
			if (t.getSubStringLength(0, x+2) <= w) {{ 
				t.textContent = txt.substring(0,x) + "..";
				return;
			}}
		}}
		t.textContent = "";
	}}
	function zoom_reset(e) {{
		if (e.attributes != undefined) {{
			orig_load(e, "x");
			orig_load(e, "width");
		}}
		if (e.childNodes == undefined) return;
		for(var i=0, c=e.childNodes; i<c.length; i++) {{
			zoom_reset(c[i]);
		}}
	}}
	function zoom_child(e, x, ratio) {{
		if (e.attributes != undefined) {{
			if (e.attributes["x"] != undefined) {{
				orig_save(e, "x");
				e.attributes["x"].value = (parseFloat(e.attributes["x"].value) - x - 10) * ratio + 10;
				if(e.tagName == "text") e.attributes["x"].value = find_child(e.parentNode, "rect", "x") + 3;
			}}
			if (e.attributes["width"] != undefined) {{
				orig_save(e, "width");
				e.attributes["width"].value = parseFloat(e.attributes["width"].value) * ratio;
			}}
		}}
		
		if (e.childNodes == undefined) return;
		for(var i=0, c=e.childNodes; i<c.length; i++) {{
			zoom_child(c[i], x-10, ratio);
		}}
	}}
	function zoom_parent(e) {{
		if (e.attributes) {{
			if (e.attributes["x"] != undefined) {{
				orig_save(e, "x");
				e.attributes["x"].value = 10;
			}}
			if (e.attributes["width"] != undefined) {{
				orig_save(e, "width");
				e.attributes["width"].value = parseInt(svg.width.baseVal.value) - (10*2);
			}}
		}}
		if (e.childNodes == undefined) return;
		for(var i=0, c=e.childNodes; i<c.length; i++) {{
			zoom_parent(c[i]);
		}}
	}}
	function zoom(node) {{ 
		var attr = find_child(node, "rect").attributes;
		var width = parseFloat(attr["width"].value);
		var xmin = parseFloat(attr["x"].value);
		var xmax = parseFloat(xmin + width);
		var ymin = parseFloat(attr["y"].value);
		var ratio = (svg.width.baseVal.value - 2*10) / width;
		
		// XXX: Workaround for JavaScript float issues (fix me)
		var fudge = 0.0001;
		
		var unzoombtn = document.getElementById("unzoom");
		unzoombtn.style["opacity"] = "1.0";
		
		var el = document.getElementsByTagName("g");
		for(var i=0;i<el.length;i++){{
			var e = el[i];
			var a = find_child(e, "rect").attributes;
			var ex = parseFloat(a["x"].value);
			var ew = parseFloat(a["width"].value);
			// Is it an ancestor
			if (0 == 0) {{
				var upstack = parseFloat(a["y"].value) > ymin;
			}} else {{
				var upstack = parseFloat(a["y"].value) < ymin;
			}}
			if (upstack) {{
				// Direct ancestor
				if (ex <= xmin && (ex+ew+fudge) >= xmax) {{
					e.style["opacity"] = "0.5";
					zoom_parent(e);
					e.onclick = function(e){{unzoom(); zoom(this);}};
					update_text(e);
				}}
				// not in current path
				else
					e.style["display"] = "none";
			}}
			// Children maybe
			else {{
				// no common path
				if (ex < xmin || ex + fudge >= xmax) {{
					e.style["display"] = "none";
				}}
				else {{
					zoom_child(e, xmin, ratio);
					e.onclick = function(e){{zoom(this);}};
					update_text(e);
				}}
			}}
		}}
	}}
	function unzoom() {{
		var unzoombtn = document.getElementById("unzoom");
		unzoombtn.style["opacity"] = "0.0";
		
		var el = document.getElementsByTagName("g");
		for(i=0;i<el.length;i++) {{
			el[i].style["display"] = "block";
			el[i].style["opacity"] = "1";
			zoom_reset(el[i]);
			update_text(el[i]);
		}}
	}}	
]]>
</script>

<rect x="0.0" y="0" width="{width}" height="{height}" fill="url(#background)"  />
<text text-anchor="middle" x="{title_x}" y="{title_y}" font-size="{title_font_size}" font-family="Verdana" fill="rgb(0,0,0)"  >{title_txt}</text>
<text text-anchor="" x="10.00" y="24" font-size="{tips_font_size}" font-family="Verdana" fill="rgb(0,0,0)" id="details" > </text>
<text text-anchor="" x="10.00" y="36" font-size="{tips_font_size}" font-family="Verdana" fill="rgb(0,0,0)" id="unzoom" onclick="unzoom()" style="opacity:0.0;cursor:pointer" >Reset Zoom</text>

{rects}

</svg>'''

ELEM = '''\
<g class="func_g" onmouseover="s('{fill}')" onmouseout="c()" onclick="zoom(this)">
<title>{fill}</title><rect x="{rect_x}" y="{rect_y}" width="{width}" height="{height}" fill="rgb({r},{g},{b})" rx="2" ry="2" />
<text text-anchor="" x="{tx}" y="{ty}" font-size="{font_size}" font-family="Verdana" fill="rgb(0,0,0)"  ></text>
</g>\n'''


class mem_node:
    def __init__(self, name, start_add, end_add, level):
        self.name = name
        self.size = end_add - start_add + 1
        self.start_add = start_add
        self.end_add = end_add
        self.childs = []
        self.parent = None
        self.level = level

        self.node_sort_index = 0
        self.rect_x = 0
        self.rect_y = 0
        self.tx = 0
        self.ty = 0
        self.width = 0

        self.r = 0
        self.g = 0
        self.b = 0

fig_cfg = {
    "title": "hello",
    "width": 1200,
    "height": 1266,
}

vaddr_size = 0xffffffffffffffff
vaddr_space = mem_node("address space", 0, 0xffffffffffffffff, 0)
mem_info_list = [[vaddr_space]]

with open("./iomem.log","+r") as f:
    space_num_list = [0]
    mem_info_list.append([])
    cur_level_list = mem_info_list[1]
    last_level = 1
    last_space_num = 0
    for line in f.readlines():
        ans = re.findall("^(\s+)([0-9a-fA-F]+)-([0-9a-fA-F]+)\s+\:\s+(.*)",line)
        if ans:
            space_num = ans[0][0].count(" ")
            start_add = int(ans[0][1], 16)
            end_add = int(ans[0][2], 16)
            name = ans[0][3]
        else:
            ans = re.findall("^([0-9a-fA-F]+)-([0-9a-fA-F]+)\s+\:\s+(.*)",line)
            if ans:
                space_num = 0
                start_add = int(ans[0][0], 16)
                end_add = int(ans[0][1], 16)
                name = ans[0][2]
            else:
                continue

        print(ans)

        if space_num not in space_num_list:
            space_num_list.append(space_num)
            last_level = len(space_num_list)
            mem_info_list.append([])
        elif space_num != last_space_num:
            for (i, n) in enumerate(space_num_list):
                if n == space_num:
                    last_level = i + 1
        node = mem_node(name, start_add, end_add, last_level)
        mem_info_list[last_level].append(node)
        print(node.name, node.size, node.level)

used_addr_size = 0
used_addr_map_param = 100
for n in mem_info_list[1]:
    used_addr_size += n.size

if used_addr_size / vaddr_size < 0.9:
    used_addr_map_param = 0.9 / used_addr_size
    print(used_addr_map_param)

rect_height = 85 / len(mem_info_list) * fig_cfg["height"] / 100
if rect_height > 0.1 * fig_cfg["height"]:
    rect_height = 0.1 * fig_cfg["height"]

print(used_addr_map_param)

s = ""
for (i,lvl) in enumerate(mem_info_list[1:]):
    rect_y = 0.95 * fig_cfg["height"] - rect_height * i
    for node in lvl:
        node.width = node.size * fig_cfg["width"]
        node.height = rect_height
        node.rect_x = node.start_add * used_addr_map_param  * fig_cfg["width"]
        node.rect_y = rect_y
        node.tx = node.rect_x + 3
        node.ty = node.rect_y + 10
        node.r = 227
        node.g = node.width * 10 % 255
        node.b = node.start_add % 125
        node.font_size = 12

        s = s + ELEM.format(fill=node.name,
                            rect_x=node.rect_x,
                            rect_y=node.rect_y,
                            tx=node.tx, ty=node.ty,
                            width=node.width,
                            height=node.height,
                            r=node.r,
                            g=node.g,
                            b=node.b,
                            font_size=node.font_size)

fig_cfg["title_x"] = fig_cfg["width"] / 2
fig_cfg["title_y"] = 20
fig_cfg["title_font_size"] = 20
fig_cfg["tips_font_size"] = 12
svg = SVG.format(title_txt=fig_cfg["title"],
             width=fig_cfg["width"],
             height=fig_cfg["height"],
             title_x=fig_cfg["title_x"],
             title_y=fig_cfg["title_y"],
             title_font_size=fig_cfg["title_font_size"],
             tips_font_size=fig_cfg["tips_font_size"],
             rects=s)
        
    
with open("./test.svg", "w") as f:
    f.write(svg)
