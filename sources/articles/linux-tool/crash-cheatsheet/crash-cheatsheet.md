---
layout: default
title: crash cheatsheet
categories: linux-tool
author: Nrush
create_time: 2024-02-23 00:36:10
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

```
# 加载dump
crash dump.202205141436 vmlinux

# 16进制查看结构体偏移
struct -ox kmem_cache

# 16进制查看结构体内成员的地址
struct -ox kmem_cache

# 查看结构成员定义
struct.kobj kmem_cache

# 查看结构percpu结构体成员
# step1 查看percpu变量偏移
struct kmem_cache.cpu_slab ffff80002a92bb00
# step2 查看percpu变量，cpu 0
struct kmem_cache_cpu 0xffff000011da4e00:0

# 遍历链表打印内嵌链表(struct list)特定值
# list -o <结构体内嵌链表节点名> -s <想查看的结构体成员用，隔开> -H <链表头地址>
list -o kmem_cache.list -s kmem_cache.name,offset -H 0xffff8000255b9860

# 遍历链表
# list -o <结构体内next指针偏移> -s <想查看的结构体成员用，隔开> <结构体地址>
list -o page.next -s page.slab_cache,flags 0xffff7e0000a4fc00

# 查看kmem_cache
kmem -s

# 查看所有page信息
kmem -p

# 查看硬件信息
mach

# 查看内存布局
mach -m
```

## Reference
