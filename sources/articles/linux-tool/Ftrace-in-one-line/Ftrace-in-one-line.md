---
layout: default
title: Ftrace in one line
categories: linux-tool
author: Nrush
create_time: 2023-04-01 20:48:32
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

```sh
perf ftrace
```

## Refrences

[1] <https://www.kernel.org/doc/html/latest/filesystems/index.html>