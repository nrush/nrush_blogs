---
layout: default
---

<center><h1>BIO</h1></center>

- Github: <https://github.com/nrusher>
- Email: <a href="mailto:jyz_nrush@163.com">jyz_nrush@163.com</a>

## Work Experience

- Operation system engineer, DJI (2021 - present)

## Website

1. Blog: <http://nr-linux.com>
