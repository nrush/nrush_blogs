---
layout: default
---

<center><h1>Links</h1></center>

Here are some websites that I think are worth paying attention to.

- The offical website of synopsys: <https://www.synopsys.com/>.
- LWN: <https://lwn.net/>.
- Brendangregg's blog: <https://www.brendangregg.com/>.
- The offical website of micron: <https://www.micron.com/>
- A website that collects many performance benchmarks: <Openbenchmarking.org>
- ARM Developer Website: <https://developer.arm.com>
