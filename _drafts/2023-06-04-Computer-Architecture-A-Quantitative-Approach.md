---
layout: default
title: Computer Architecture A Quantitative Approach
categories: reading-note
author: Nrush
create_time: 2023-06-04 22:06:46
---
<center><strong><font size=6>{{page.title}}</font></strong></center>

- this unordered seed list will be replaced by toc as unordered list
{:toc}

### Classes of Parallelism and Parallel Architectures
parallelism in applications:
1. Data-Level Parallelism (DLP) arises because there are many data items that
can be operated on at the same time

2. Task-Level Parallelism (TLP) arises because tasks of work are created that
can operate independently and largely in parallel.

Computer hardware in turn can exploit these two kinds of application parallelism
in four major ways:

1. Instruction-Level Parallelism exploits data-level parallelism at modest levels
with compiler help using ideas like pipelining and at medium levels using
ideas like speculative execution.
2. Vector Architectures and Graphic Processor Units (GPUs) exploit data-level
parallelism by applying a single instruction to a collection of data in parallel.
3. Thread-Level Parallelism exploits either data-level parallelism or task-level
parallelism in a tightly coupled hardware model that allows for interaction
among parallel threads.
4. Request-Level Parallelism exploits parallelism among largely decoupled
tasks specified by the programmer or the operating system.

He looked at the parallelism in the
instruction and data streams called for by the instructions at the most con-
strained component of the multiprocessor, and placed all computers into one of
four categories:

1. Single instruction stream, single data stream (SISD)—This category is the
uniprocessor. The programmer thinks of it as the standard sequential com-
puter, but it can exploit instruction-level parallelism. Chapter 3 covers SISD
architectures that use ILP techniques such as superscalar and speculative exe-
cution.
2. Single instruction stream, multiple data streams (SIMD)—The same
instruction is executed by multiple processors using different data streams.
SIMD computers exploit data-level parallelism by applying the same
operations to multiple items of data in parallel. Each processor has its own
data memory (hence the MD of SIMD), but there is a single instruction
memory and control processor, which fetches and dispatches instructions.
Chapter 4 covers DLP and three different architectures that exploit it:
vector architectures, multimedia extensions to standard instruction sets,
and GPUs.
3. Multiple instruction streams, single data stream (MISD)—No commercial
multiprocessor of this type has been built to date, but it rounds out this simple
classification.
4. Multiple instruction streams, multiple data streams (MIMD)—Each proces-
sor fetches its own instructions and operates on its own data, and it targets
task-level parallelism. In general, MIMD is more flexible than SIMD and
thus more generally applicable, but it is inherently more expensive than
SIMD. For example, MIMD computers can also exploit data-level parallel-
ism, although the overhead is likely to be higher than would be seen in an
SIMD computer. This overhead means that grain size must be sufficiently
large to exploit the parallelism efficiently. Chapter 5 covers tightly coupled
MIMD architectures, which exploit thread-level parallelism since multiple
cooperating threads operate in parallel. Chapter 6 covers loosely coupled
MIMD architectures—specifically, clusters and warehouse-scale comput-
ers—that exploit request-level parallelism, where many independent tasks
can proceed in parallel naturally with little need for communication or
synchronization.

## 1.3 Defining Computer Architecture

Determine what attributes are important for a new computer, then design a computer to maximize
performance and energy efficiency while staying within cost, power, and avail-
ability constraints

instruction set architecture (ISA)

## 1.4 Trends in Technology

bandwidth or throughput is the total amount of work done in a given time.

latency or response time is the time between the start and the completion of an event

A simple rule of thumb is that bandwidth grows by at least the square of the improvement in latency.

## 1.5 Trends in Power and Energy in Integrated Circuits

Today, power is the biggest challenge facing the computer designer for nearly
every class of computer. First, power must be brought in and distributed around
the chip, and modern microprocessors use hundreds of pins and multiple inter-
connect layers just for power and ground. Second, power is dissipated as heat and
must be removed.

$$E_{dynamic 0->1} \propto 1/2 \times C \times V^{2} $$

$$P_{dynamic} \propto 1/2 \times C \times V^{2} \times F$$

$$P_{static} \propto I_{static} V$$

## 1.6 Trends in Cost

die越小，晶圆越大越便宜

## 1.8 Measuring, Reporting, and Summarizing Performance


## Reference
